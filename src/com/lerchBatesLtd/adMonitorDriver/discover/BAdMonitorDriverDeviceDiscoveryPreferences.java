/*
 * Copyright 2014 Lerch Bates Ltd, All Rights Reserved.
 */

package com.lerchBatesLtd.adMonitorDriver.discover;

import javax.baja.sys.*;

import com.tridium.ddf.identify.*;
import com.tridium.ddf.discover.auto.*;

import com.lerchBatesLtd.adMonitorDriver.identify.*;

public class BAdMonitorDriverDeviceDiscoveryPreferences
  extends BDdfAutoDiscoveryPreferences
{
  /*-
  class BAdMonitorDriverDeviceDiscoveryPreferences
  {
    properties
    {
      timeout : BRelTime
        -- This is the amount of time to wait per field-bus request before timing out
        default{[BRelTime.makeSeconds(3)]}
        slotfacets{[BFacets.make(BFacets.make(BFacets.SHOW_MILLISECONDS,BBoolean.TRUE),
                                 BFacets.MIN,BRelTime.make(0))]}
      retryCount : int
        -- This is the number of discovery field-message retransmissions
        -- per request.
        default{[1]}
        slotfacets{[BFacets.make(BFacets.MIN,BInteger.make(0))]}
      min : BDdfIdParams
        -- This is the id of the lowest device for your driver to attempt to
        -- learn by default
                        default{[(BDdfIdParams)new BAdMonitorDriverDeviceDiscoverParams().getFirst()]}
        
      max : BDdfIdParams
        -- This is the id of the highest device for your driver to attempt to
        -- learn by default
                        default{[(BDdfIdParams)new BAdMonitorDriverDeviceDiscoverParams().getLast()]}
            }
  }
  -*/
/*+ ------------ BEGIN BAJA AUTO GENERATED CODE ------------ +*/
/*@ $com.lerchBatesLtd.adMonitorDriver.discover.BAdMonitorDriverDeviceDiscoveryPreferences(3518614438)1.0$ @*/
/* Generated Tue Jan 21 13:08:13 GMT 2014 by Slot-o-Matic 2000 (c) Tridium, Inc. 2000 */

////////////////////////////////////////////////////////////////
// Property "timeout"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the <code>timeout</code> property.
   * This is the amount of time to wait per field-bus request
   * before timing out
   * @see com.lerchBatesLtd.adMonitorDriver.discover.BAdMonitorDriverDeviceDiscoveryPreferences#getTimeout
   * @see com.lerchBatesLtd.adMonitorDriver.discover.BAdMonitorDriverDeviceDiscoveryPreferences#setTimeout
   */
  public static final Property timeout = newProperty(0, BRelTime.makeSeconds(3),BFacets.make(BFacets.make(BFacets.SHOW_MILLISECONDS,BBoolean.TRUE),
                                 BFacets.MIN,BRelTime.make(0)));
  
  /**
   * Get the <code>timeout</code> property.
   * This is the amount of time to wait per field-bus request
   * before timing out
   * @see com.lerchBatesLtd.adMonitorDriver.discover.BAdMonitorDriverDeviceDiscoveryPreferences#timeout
   */
  public BRelTime getTimeout() { return (BRelTime)get(timeout); }
  
  /**
   * Set the <code>timeout</code> property.
   * This is the amount of time to wait per field-bus request
   * before timing out
   * @see com.lerchBatesLtd.adMonitorDriver.discover.BAdMonitorDriverDeviceDiscoveryPreferences#timeout
   */
  public void setTimeout(BRelTime v) { set(timeout,v,null); }

////////////////////////////////////////////////////////////////
// Property "retryCount"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the <code>retryCount</code> property.
   * This is the number of discovery field-message retransmissions
   * per request.
   * @see com.lerchBatesLtd.adMonitorDriver.discover.BAdMonitorDriverDeviceDiscoveryPreferences#getRetryCount
   * @see com.lerchBatesLtd.adMonitorDriver.discover.BAdMonitorDriverDeviceDiscoveryPreferences#setRetryCount
   */
  public static final Property retryCount = newProperty(0, 1,BFacets.make(BFacets.MIN,BInteger.make(0)));
  
  /**
   * Get the <code>retryCount</code> property.
   * This is the number of discovery field-message retransmissions
   * per request.
   * @see com.lerchBatesLtd.adMonitorDriver.discover.BAdMonitorDriverDeviceDiscoveryPreferences#retryCount
   */
  public int getRetryCount() { return getInt(retryCount); }
  
  /**
   * Set the <code>retryCount</code> property.
   * This is the number of discovery field-message retransmissions
   * per request.
   * @see com.lerchBatesLtd.adMonitorDriver.discover.BAdMonitorDriverDeviceDiscoveryPreferences#retryCount
   */
  public void setRetryCount(int v) { setInt(retryCount,v,null); }

////////////////////////////////////////////////////////////////
// Property "min"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the <code>min</code> property.
   * This is the id of the lowest device for your driver
   * to attempt to learn by default
   * @see com.lerchBatesLtd.adMonitorDriver.discover.BAdMonitorDriverDeviceDiscoveryPreferences#getMin
   * @see com.lerchBatesLtd.adMonitorDriver.discover.BAdMonitorDriverDeviceDiscoveryPreferences#setMin
   */
  public static final Property min = newProperty(0, (BDdfIdParams)new BAdMonitorDriverDeviceDiscoverParams().getFirst(),null);
  
  /**
   * Get the <code>min</code> property.
   * This is the id of the lowest device for your driver
   * to attempt to learn by default
   * @see com.lerchBatesLtd.adMonitorDriver.discover.BAdMonitorDriverDeviceDiscoveryPreferences#min
   */
  public BDdfIdParams getMin() { return (BDdfIdParams)get(min); }
  
  /**
   * Set the <code>min</code> property.
   * This is the id of the lowest device for your driver
   * to attempt to learn by default
   * @see com.lerchBatesLtd.adMonitorDriver.discover.BAdMonitorDriverDeviceDiscoveryPreferences#min
   */
  public void setMin(BDdfIdParams v) { set(min,v,null); }

////////////////////////////////////////////////////////////////
// Property "max"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the <code>max</code> property.
   * This is the id of the highest device for your driver
   * to attempt to learn by default
   * @see com.lerchBatesLtd.adMonitorDriver.discover.BAdMonitorDriverDeviceDiscoveryPreferences#getMax
   * @see com.lerchBatesLtd.adMonitorDriver.discover.BAdMonitorDriverDeviceDiscoveryPreferences#setMax
   */
  public static final Property max = newProperty(0, (BDdfIdParams)new BAdMonitorDriverDeviceDiscoverParams().getLast(),null);
  
  /**
   * Get the <code>max</code> property.
   * This is the id of the highest device for your driver
   * to attempt to learn by default
   * @see com.lerchBatesLtd.adMonitorDriver.discover.BAdMonitorDriverDeviceDiscoveryPreferences#max
   */
  public BDdfIdParams getMax() { return (BDdfIdParams)get(max); }
  
  /**
   * Set the <code>max</code> property.
   * This is the id of the highest device for your driver
   * to attempt to learn by default
   * @see com.lerchBatesLtd.adMonitorDriver.discover.BAdMonitorDriverDeviceDiscoveryPreferences#max
   */
  public void setMax(BDdfIdParams v) { set(max,v,null); }

////////////////////////////////////////////////////////////////
// Type
////////////////////////////////////////////////////////////////
  
  public Type getType() { return TYPE; }
  public static final Type TYPE = Sys.loadType(BAdMonitorDriverDeviceDiscoveryPreferences.class);

/*+ ------------ END BAJA AUTO GENERATED CODE -------------- +*/
}