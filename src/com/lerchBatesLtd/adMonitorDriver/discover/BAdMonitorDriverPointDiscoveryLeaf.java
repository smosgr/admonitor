/*
 * Copyright 2014 Lerch Bates Ltd, All Rights Reserved.
 */
package com.lerchBatesLtd.adMonitorDriver.discover; 
 

import javax.baja.control.*;
import javax.baja.registry.TypeInfo;
import javax.baja.sys.*;

import com.tridium.ddf.identify.*; 
import com.tridium.ddf.discover.*; 
import com.lerchBatesLtd.adMonitorDriver.identify.*; 
  
public class BAdMonitorDriverPointDiscoveryLeaf 
  extends BDdfPointDiscoveryLeaf 
{ 
  /*- 
  class BAdMonitorDriverPointDiscoveryLeaf 
  { 
    properties 
    { 
      pointId : BDdfIdParams 
        default{[new BAdMonitorDriverPointId()]} 
        slotfacets{[MGR_INCLUDE]} 
      readParameters : BDdfIdParams 
        default{[new BAdMonitorDriverReadParams()]} 
        slotfacets{[MGR_INCLUDE]} 
      writeParameters : BDdfIdParams 
        default{[new BAdMonitorDriverWriteParams()]} 
        slotfacets{[MGR_INCLUDE]} 
    } 
  } 
  -*/
/*+ ------------ BEGIN BAJA AUTO GENERATED CODE ------------ +*/
/*@ $com.lerchBatesLtd.adMonitorDriver.discover.BAdMonitorDriverPointDiscoveryLeaf(3042819043)1.0$ @*/
/* Generated Tue Jan 21 13:08:13 GMT 2014 by Slot-o-Matic 2000 (c) Tridium, Inc. 2000 */

////////////////////////////////////////////////////////////////
// Property "pointId"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the <code>pointId</code> property.
   * @see com.lerchBatesLtd.adMonitorDriver.discover.BAdMonitorDriverPointDiscoveryLeaf#getPointId
   * @see com.lerchBatesLtd.adMonitorDriver.discover.BAdMonitorDriverPointDiscoveryLeaf#setPointId
   */
  public static final Property pointId = newProperty(0, new BAdMonitorDriverPointId(),MGR_INCLUDE);
  
  /**
   * Get the <code>pointId</code> property.
   * @see com.lerchBatesLtd.adMonitorDriver.discover.BAdMonitorDriverPointDiscoveryLeaf#pointId
   */
  public BDdfIdParams getPointId() { return (BDdfIdParams)get(pointId); }
  
  /**
   * Set the <code>pointId</code> property.
   * @see com.lerchBatesLtd.adMonitorDriver.discover.BAdMonitorDriverPointDiscoveryLeaf#pointId
   */
  public void setPointId(BDdfIdParams v) { set(pointId,v,null); }

////////////////////////////////////////////////////////////////
// Property "readParameters"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the <code>readParameters</code> property.
   * @see com.lerchBatesLtd.adMonitorDriver.discover.BAdMonitorDriverPointDiscoveryLeaf#getReadParameters
   * @see com.lerchBatesLtd.adMonitorDriver.discover.BAdMonitorDriverPointDiscoveryLeaf#setReadParameters
   */
  public static final Property readParameters = newProperty(0, new BAdMonitorDriverReadParams(),MGR_INCLUDE);
  
  /**
   * Get the <code>readParameters</code> property.
   * @see com.lerchBatesLtd.adMonitorDriver.discover.BAdMonitorDriverPointDiscoveryLeaf#readParameters
   */
  public BDdfIdParams getReadParameters() { return (BDdfIdParams)get(readParameters); }
  
  /**
   * Set the <code>readParameters</code> property.
   * @see com.lerchBatesLtd.adMonitorDriver.discover.BAdMonitorDriverPointDiscoveryLeaf#readParameters
   */
  public void setReadParameters(BDdfIdParams v) { set(readParameters,v,null); }

////////////////////////////////////////////////////////////////
// Property "writeParameters"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the <code>writeParameters</code> property.
   * @see com.lerchBatesLtd.adMonitorDriver.discover.BAdMonitorDriverPointDiscoveryLeaf#getWriteParameters
   * @see com.lerchBatesLtd.adMonitorDriver.discover.BAdMonitorDriverPointDiscoveryLeaf#setWriteParameters
   */
  public static final Property writeParameters = newProperty(0, new BAdMonitorDriverWriteParams(),MGR_INCLUDE);
  
  /**
   * Get the <code>writeParameters</code> property.
   * @see com.lerchBatesLtd.adMonitorDriver.discover.BAdMonitorDriverPointDiscoveryLeaf#writeParameters
   */
  public BDdfIdParams getWriteParameters() { return (BDdfIdParams)get(writeParameters); }
  
  /**
   * Set the <code>writeParameters</code> property.
   * @see com.lerchBatesLtd.adMonitorDriver.discover.BAdMonitorDriverPointDiscoveryLeaf#writeParameters
   */
  public void setWriteParameters(BDdfIdParams v) { set(writeParameters,v,null); }

////////////////////////////////////////////////////////////////
// Type
////////////////////////////////////////////////////////////////
  
  public Type getType() { return TYPE; }
  public static final Type TYPE = Sys.loadType(BAdMonitorDriverPointDiscoveryLeaf.class);

/*+ ------------ END BAJA AUTO GENERATED CODE -------------- +*/ 
 
 
  /** 
   * When a control point is added to the station from the 
   * Point Manager, it is given this name by default (possibly 
   * with a suffix to make it unique). 
   */ 
  public String getDiscoveryName() 
  {
    // TODO: Return a string to serve as the default name for this
    // item if the end-user selects this item's row in the
    // "Discovered" list on the point point manager and clicks the
    // "Add" button to add this item to the station database.
    // Good luck.
    
    
    BAdMonitorDriverPointId  pointId = ((BAdMonitorDriverPointId)getPointId());   
    return "Point_"+pointId.getOffset(); // TO CHANGE!
  }
  
  
  public TypeInfo[] getValidDatabaseTypes()
  {
    
    return new TypeInfo[]{BEnumWritable.TYPE.getTypeInfo()}; // in this way we accept only this kind of type --> BEnumWritable
  //In our case we will accept more types.
  }
  
  //public BFacets getDiscoveryPointFacets()                           // toChange!!
 // {
   // BEnumRange range = BTrafficLightColor.green.getRange();
  //  return BFacets.make(BFacets.RANGE, range);
 // }
  
}