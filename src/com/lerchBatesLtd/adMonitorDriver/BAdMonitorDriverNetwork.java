/*
 * Copyright 2014 Lerch Bates Ltd, All Rights Reserved.
 */
package com.lerchBatesLtd.adMonitorDriver;

import javax.baja.sys.BValue;
import javax.baja.sys.Property;
import javax.baja.sys.Sys;
import javax.baja.sys.Type;

import com.tridium.ddf.discover.BDdfDiscoveryPreferences;

import com.tridium.ddfSerial.BDdfSerialNetwork;
import com.lerchBatesLtd.adMonitorDriver.comm.BAdMonitorDriverCommunicator;
import com.lerchBatesLtd.adMonitorDriver.discover.*;


public class BAdMonitorDriverNetwork
   extends BDdfSerialNetwork{
  /*-
  class BAdMonitorDriverNetwork
  {
    properties
    {
      communicator : BValue
        -- This plugs in an instance of yourDriver's
        -- communicator onto the serial network component.
        -- The Niagara station's platform will communicate
        -- over a serial port that is configured on this
        -- serial network. You can look at the property
        -- sheet of this communicator to review the exact
        -- settings.
        default{[ new BAdMonitorDriverCommunicator() ]}
      discoveryPreferences : BDdfDiscoveryPreferences
        -- This saves the last set of discovery preferences
        -- that the user provides on the device manager. It
        -- is also used as the default for the first Time
        -- that the user is prompted for a discovery.
        default{[ new BAdMonitorDriverDeviceDiscoveryPreferences()]}
    }
  }
  -*/
/*+ ------------ BEGIN BAJA AUTO GENERATED CODE ------------ +*/
/*@ $com.lerchBatesLtd.adMonitorDriver.BAdMonitorDriverNetwork(3004434171)1.0$ @*/
/* Generated Tue Jan 21 13:08:13 GMT 2014 by Slot-o-Matic 2000 (c) Tridium, Inc. 2000 */

////////////////////////////////////////////////////////////////
// Property "communicator"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the <code>communicator</code> property.
   * This plugs in an instance of yourDriver's communicator
   * onto the serial network component. The Niagara station's
   * platform will communicate over a serial port that is configured on this serial network. You can look at the property sheet of this communicator to review the exact settings.
   * @see com.lerchBatesLtd.adMonitorDriver.BAdMonitorDriverNetwork#getCommunicator
   * @see com.lerchBatesLtd.adMonitorDriver.BAdMonitorDriverNetwork#setCommunicator
   */
  public static final Property communicator = newProperty(0, new BAdMonitorDriverCommunicator(),null);
  
  /**
   * Get the <code>communicator</code> property.
   * This plugs in an instance of yourDriver's communicator
   * onto the serial network component. The Niagara station's
   * platform will communicate over a serial port that is configured on this serial network. You can look at the property sheet of this communicator to review the exact settings.
   * @see com.lerchBatesLtd.adMonitorDriver.BAdMonitorDriverNetwork#communicator
   */
  public BValue getCommunicator() { return (BValue)get(communicator); }
  
  /**
   * Set the <code>communicator</code> property.
   * This plugs in an instance of yourDriver's communicator
   * onto the serial network component. The Niagara station's
   * platform will communicate over a serial port that is configured on this serial network. You can look at the property sheet of this communicator to review the exact settings.
   * @see com.lerchBatesLtd.adMonitorDriver.BAdMonitorDriverNetwork#communicator
   */
  public void setCommunicator(BValue v) { set(communicator,v,null); }

////////////////////////////////////////////////////////////////
// Property "discoveryPreferences"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the <code>discoveryPreferences</code> property.
   * This saves the last set of discovery preferences that
   * the user provides on the device manager. It is also
   * used as the default for the first Time that the user
   * is prompted for a discovery.
   * @see com.lerchBatesLtd.adMonitorDriver.BAdMonitorDriverNetwork#getDiscoveryPreferences
   * @see com.lerchBatesLtd.adMonitorDriver.BAdMonitorDriverNetwork#setDiscoveryPreferences
   */
  public static final Property discoveryPreferences = newProperty(0, new BAdMonitorDriverDeviceDiscoveryPreferences(),null);
  
  /**
   * Get the <code>discoveryPreferences</code> property.
   * This saves the last set of discovery preferences that
   * the user provides on the device manager. It is also
   * used as the default for the first Time that the user
   * is prompted for a discovery.
   * @see com.lerchBatesLtd.adMonitorDriver.BAdMonitorDriverNetwork#discoveryPreferences
   */
  public BDdfDiscoveryPreferences getDiscoveryPreferences() { return (BDdfDiscoveryPreferences)get(discoveryPreferences); }
  
  /**
   * Set the <code>discoveryPreferences</code> property.
   * This saves the last set of discovery preferences that
   * the user provides on the device manager. It is also
   * used as the default for the first Time that the user
   * is prompted for a discovery.
   * @see com.lerchBatesLtd.adMonitorDriver.BAdMonitorDriverNetwork#discoveryPreferences
   */
  public void setDiscoveryPreferences(BDdfDiscoveryPreferences v) { set(discoveryPreferences,v,null); }

////////////////////////////////////////////////////////////////
// Type
////////////////////////////////////////////////////////////////
  
  public Type getType() { return TYPE; }
  public static final Type TYPE = Sys.loadType(BAdMonitorDriverNetwork.class);

/*+ ------------ END BAJA AUTO GENERATED CODE -------------- +*/

  public Type getDeviceType()
  {
    return BAdMonitorDriverDevice.TYPE;
  }
  
  public Type getDeviceFolderType()
  {
    return BAdMonitorDriverDeviceFolder.TYPE;
  }
}