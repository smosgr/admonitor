/*
 * Copyright 2014 Lerch Bates Ltd, All Rights Reserved.
 */
package com.lerchBatesLtd.adMonitorDriver.point;

import javax.baja.sys.*; 
 
import com.tridium.ddf.*; 
  
public class BAdMonitorDriverPointFolder 
  extends BDdfPointFolder 
{ 
  /*- 
  class BAdMonitorDriverPointFolder 
  { 
    properties 
    { 
    } 
  } 
  -*/
/*+ ------------ BEGIN BAJA AUTO GENERATED CODE ------------ +*/
/*@ $com.lerchBatesLtd.adMonitorDriver.point.BAdMonitorDriverPointFolder(2886615974)1.0$ @*/
/* Generated Tue Jan 21 13:08:13 GMT 2014 by Slot-o-Matic 2000 (c) Tridium, Inc. 2000 */

////////////////////////////////////////////////////////////////
// Type
////////////////////////////////////////////////////////////////
  
  public Type getType() { return TYPE; }
  public static final Type TYPE = Sys.loadType(BAdMonitorDriverPointFolder.class);

/*+ ------------ END BAJA AUTO GENERATED CODE -------------- +*/ 
}