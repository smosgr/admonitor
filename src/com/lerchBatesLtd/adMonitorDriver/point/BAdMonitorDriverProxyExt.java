/*
 * Copyright 2014 Lerch Bates Ltd, All Rights Reserved.
 */
package com.lerchBatesLtd.adMonitorDriver.point;

import javax.baja.driver.util.BPollFrequency;
import javax.baja.sys.Property;
import javax.baja.sys.Sys;
import javax.baja.sys.Type;

import com.lerchBatesLtd.adMonitorDriver.BAdMonitorDriverPointDeviceExt;
import com.lerchBatesLtd.adMonitorDriver.identify.BAdMonitorDriverPointId;
import com.lerchBatesLtd.adMonitorDriver.identify.BAdMonitorDriverReadParams;
import com.lerchBatesLtd.adMonitorDriver.identify.BAdMonitorDriverWriteParams;
import com.tridium.ddf.identify.BDdfIdParams;
import com.tridium.ddf.point.BDdfProxyExt;


public class BAdMonitorDriverProxyExt
  extends BDdfProxyExt
{
  /*-
  class BAdMonitorDriverProxyExt
  {
    properties
    {
       readParameters : BDdfIdParams
         -- This hooks test driver's read parameters structure into the
         -- proxy extension that is placed on control points that are
         -- under devices in your driver. The read parameter's structure
         -- tells the dev driver framework which read request to use to
         -- read the control point. It also tells test read request's
         -- toByteArray method how to construct the bytes for the request.
         default{[new BAdMonitorDriverReadParams()]}
         slotfacets{[MGR_INCLUDE]}
       pointId : BDdfIdParams
         -- This tells your driver's read and write response's
         -- parseReadValue method how to extract the data value for a
         -- particular control point.
         default{[new BAdMonitorDriverPointId()]}
         slotfacets{[MGR_INCLUDE]}
       writeParameters : BDdfIdParams 
         -- This hooks your driver's write parameters structure into the 
         -- proxy extension that is placed on control points that are 
         -- under devices in your driver. The write parameter's structure 
         -- tells the dev driver framework which write request to use to 
         -- write the control point. It also tells your write request's 
         -- toByteArray method how to construct the bytes for the request. 
         default{[new BAdMonitorDriverWriteParams()]} 
         slotfacets{[MGR_INCLUDE]}      
         
             
             
       pollFrequency : BPollFrequency
           default {[ BPollFrequency.normal ]}
           
           
       -- deviceFacets: BFacets
        --  default {[ BFacets.make(BFacets.RANGE, BTrafficLightColor.red.getRange())]}
    }
  }
  -*/
/*+ ------------ BEGIN BAJA AUTO GENERATED CODE ------------ +*/
/*@ $com.lerchBatesLtd.adMonitorDriver.point.BAdMonitorDriverProxyExt(140131985)1.0$ @*/
/* Generated Tue Jan 21 18:52:26 GMT 2014 by Slot-o-Matic 2000 (c) Tridium, Inc. 2000 */

////////////////////////////////////////////////////////////////
// Property "readParameters"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the <code>readParameters</code> property.
   * This hooks test driver's read parameters structure
   * into the proxy extension that is placed on control
   * points that are under devices in your driver. The read parameter's structure tells the dev driver framework which read request to use to read the control point. It also tells test read request's toByteArray method how to construct the bytes for the request.
   * @see com.lerchBatesLtd.adMonitorDriver.point.BAdMonitorDriverProxyExt#getReadParameters
   * @see com.lerchBatesLtd.adMonitorDriver.point.BAdMonitorDriverProxyExt#setReadParameters
   */
  public static final Property readParameters = newProperty(0, new BAdMonitorDriverReadParams(),MGR_INCLUDE);
  
  /**
   * Get the <code>readParameters</code> property.
   * This hooks test driver's read parameters structure
   * into the proxy extension that is placed on control
   * points that are under devices in your driver. The read parameter's structure tells the dev driver framework which read request to use to read the control point. It also tells test read request's toByteArray method how to construct the bytes for the request.
   * @see com.lerchBatesLtd.adMonitorDriver.point.BAdMonitorDriverProxyExt#readParameters
   */
  public BDdfIdParams getReadParameters() { return (BDdfIdParams)get(readParameters); }
  
  /**
   * Set the <code>readParameters</code> property.
   * This hooks test driver's read parameters structure
   * into the proxy extension that is placed on control
   * points that are under devices in your driver. The read parameter's structure tells the dev driver framework which read request to use to read the control point. It also tells test read request's toByteArray method how to construct the bytes for the request.
   * @see com.lerchBatesLtd.adMonitorDriver.point.BAdMonitorDriverProxyExt#readParameters
   */
  public void setReadParameters(BDdfIdParams v) { set(readParameters,v,null); }

////////////////////////////////////////////////////////////////
// Property "pointId"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the <code>pointId</code> property.
   * This tells your driver's read and write response's
   * parseReadValue method how to extract the data value
   * for a particular control point.
   * @see com.lerchBatesLtd.adMonitorDriver.point.BAdMonitorDriverProxyExt#getPointId
   * @see com.lerchBatesLtd.adMonitorDriver.point.BAdMonitorDriverProxyExt#setPointId
   */
  public static final Property pointId = newProperty(0, new BAdMonitorDriverPointId(),MGR_INCLUDE);
  
  /**
   * Get the <code>pointId</code> property.
   * This tells your driver's read and write response's
   * parseReadValue method how to extract the data value
   * for a particular control point.
   * @see com.lerchBatesLtd.adMonitorDriver.point.BAdMonitorDriverProxyExt#pointId
   */
  public BDdfIdParams getPointId() { return (BDdfIdParams)get(pointId); }
  
  /**
   * Set the <code>pointId</code> property.
   * This tells your driver's read and write response's
   * parseReadValue method how to extract the data value
   * for a particular control point.
   * @see com.lerchBatesLtd.adMonitorDriver.point.BAdMonitorDriverProxyExt#pointId
   */
  public void setPointId(BDdfIdParams v) { set(pointId,v,null); }

////////////////////////////////////////////////////////////////
// Property "writeParameters"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the <code>writeParameters</code> property.
   * This hooks your driver's write parameters structure
   * into the proxy extension that is placed on control
   * points that are under devices in your driver. The write parameter's structure tells the dev driver framework which write request to use to write the control point. It also tells your write request's toByteArray method how to construct the bytes for the request.
   * @see com.lerchBatesLtd.adMonitorDriver.point.BAdMonitorDriverProxyExt#getWriteParameters
   * @see com.lerchBatesLtd.adMonitorDriver.point.BAdMonitorDriverProxyExt#setWriteParameters
   */
  public static final Property writeParameters = newProperty(0, new BAdMonitorDriverWriteParams(),MGR_INCLUDE);
  
  /**
   * Get the <code>writeParameters</code> property.
   * This hooks your driver's write parameters structure
   * into the proxy extension that is placed on control
   * points that are under devices in your driver. The write parameter's structure tells the dev driver framework which write request to use to write the control point. It also tells your write request's toByteArray method how to construct the bytes for the request.
   * @see com.lerchBatesLtd.adMonitorDriver.point.BAdMonitorDriverProxyExt#writeParameters
   */
  public BDdfIdParams getWriteParameters() { return (BDdfIdParams)get(writeParameters); }
  
  /**
   * Set the <code>writeParameters</code> property.
   * This hooks your driver's write parameters structure
   * into the proxy extension that is placed on control
   * points that are under devices in your driver. The write parameter's structure tells the dev driver framework which write request to use to write the control point. It also tells your write request's toByteArray method how to construct the bytes for the request.
   * @see com.lerchBatesLtd.adMonitorDriver.point.BAdMonitorDriverProxyExt#writeParameters
   */
  public void setWriteParameters(BDdfIdParams v) { set(writeParameters,v,null); }

////////////////////////////////////////////////////////////////
// Property "pollFrequency"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the <code>pollFrequency</code> property.
   * @see com.lerchBatesLtd.adMonitorDriver.point.BAdMonitorDriverProxyExt#getPollFrequency
   * @see com.lerchBatesLtd.adMonitorDriver.point.BAdMonitorDriverProxyExt#setPollFrequency
   */
  public static final Property pollFrequency = newProperty(0, BPollFrequency.normal,null);
  
  /**
   * Get the <code>pollFrequency</code> property.
   * @see com.lerchBatesLtd.adMonitorDriver.point.BAdMonitorDriverProxyExt#pollFrequency
   */
  public BPollFrequency getPollFrequency() { return (BPollFrequency)get(pollFrequency); }
  
  /**
   * Set the <code>pollFrequency</code> property.
   * @see com.lerchBatesLtd.adMonitorDriver.point.BAdMonitorDriverProxyExt#pollFrequency
   */
  public void setPollFrequency(BPollFrequency v) { set(pollFrequency,v,null); }

////////////////////////////////////////////////////////////////
// Type
////////////////////////////////////////////////////////////////
  
  public Type getType() { return TYPE; }
  public static final Type TYPE = Sys.loadType(BAdMonitorDriverProxyExt.class);

/*+ ------------ END BAJA AUTO GENERATED CODE -------------- +*/



  /**
   * This associates BAdMonitorDriverDeviceExt with
   * BAdMonitorDriverProxyExt within the Niagara AX
   * framework.
   */
  public Type getDeviceExtType()
  {
    return BAdMonitorDriverPointDeviceExt.TYPE;
  }
}