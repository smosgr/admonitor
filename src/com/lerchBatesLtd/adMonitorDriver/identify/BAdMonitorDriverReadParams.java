/*
 * Copyright 2014 Lerch Bates Ltd, All Rights Reserved.
 */
package com.lerchBatesLtd.adMonitorDriver.identify;

import javax.baja.sys.Property;
import javax.baja.sys.Sys;
import javax.baja.sys.Type;

import com.tridium.ddf.identify.*; 

import com.tridium.ddf.identify.BDdfReadParams;
import com.tridium.ddf.identify.BIDdfDiscoverParams;

import com.lerchBatesLtd.adMonitorDriver.comm.req.BAdMonitorDriverReadRequest;
import com.lerchBatesLtd.adMonitorDriver.discover.BAdMonitorDriverPointDiscoveryLeaf;
import com.lerchBatesLtd.adMonitorDriver.identify.*;

public class BAdMonitorDriverReadParams
  extends BDdfReadParams
  {
  /*-
  class BAdMonitorDriverReadParams
  {
    properties
    {
      typeString : String
        -- This property has nothing to with the dev
        -- driver framework itself. Instead, we need
        -- to construct the toByteArray method of the
        -- driver's read request in following the
        -- driver's protocol to read data values.
        default{["analog"]}
        slotfacets{[MGR_INCLUDE]}

      direction : String
        -- This property has nothing to with the ev
        -- driver framework itself. Instead, we need
        -- to construct the toByteArray method of the
        -- driver's read request in following the
        -- driver's protocol to read data values.
        default{["outputs"]}
        slotfacets{[MGR_INCLUDE]}
    }
  }
  -*/
/*+ ------------ BEGIN BAJA AUTO GENERATED CODE ------------ +*/
/*@ $com.lerchBatesLtd.adMonitorDriver.identify.BAdMonitorDriverReadParams(2708840841)1.0$ @*/
/* Generated Tue Jan 21 13:08:13 GMT 2014 by Slot-o-Matic 2000 (c) Tridium, Inc. 2000 */

////////////////////////////////////////////////////////////////
// Property "typeString"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the <code>typeString</code> property.
   * This property has nothing to with the dev driver framework
   * itself. Instead, we need to construct the toByteArray
   * method of the driver's read request in following the
   * driver's protocol to read data values.
   * @see com.lerchBatesLtd.adMonitorDriver.identify.BAdMonitorDriverReadParams#getTypeString
   * @see com.lerchBatesLtd.adMonitorDriver.identify.BAdMonitorDriverReadParams#setTypeString
   */
  public static final Property typeString = newProperty(0, "analog",MGR_INCLUDE);
  
  /**
   * Get the <code>typeString</code> property.
   * This property has nothing to with the dev driver framework
   * itself. Instead, we need to construct the toByteArray
   * method of the driver's read request in following the
   * driver's protocol to read data values.
   * @see com.lerchBatesLtd.adMonitorDriver.identify.BAdMonitorDriverReadParams#typeString
   */
  public String getTypeString() { return getString(typeString); }
  
  /**
   * Set the <code>typeString</code> property.
   * This property has nothing to with the dev driver framework
   * itself. Instead, we need to construct the toByteArray
   * method of the driver's read request in following the
   * driver's protocol to read data values.
   * @see com.lerchBatesLtd.adMonitorDriver.identify.BAdMonitorDriverReadParams#typeString
   */
  public void setTypeString(String v) { setString(typeString,v,null); }

////////////////////////////////////////////////////////////////
// Property "direction"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the <code>direction</code> property.
   * This property has nothing to with the ev driver framework
   * itself. Instead, we need to construct the toByteArray
   * method of the driver's read request in following the
   * driver's protocol to read data values.
   * @see com.lerchBatesLtd.adMonitorDriver.identify.BAdMonitorDriverReadParams#getDirection
   * @see com.lerchBatesLtd.adMonitorDriver.identify.BAdMonitorDriverReadParams#setDirection
   */
  public static final Property direction = newProperty(0, "outputs",MGR_INCLUDE);
  
  /**
   * Get the <code>direction</code> property.
   * This property has nothing to with the ev driver framework
   * itself. Instead, we need to construct the toByteArray
   * method of the driver's read request in following the
   * driver's protocol to read data values.
   * @see com.lerchBatesLtd.adMonitorDriver.identify.BAdMonitorDriverReadParams#direction
   */
  public String getDirection() { return getString(direction); }
  
  /**
   * Set the <code>direction</code> property.
   * This property has nothing to with the ev driver framework
   * itself. Instead, we need to construct the toByteArray
   * method of the driver's read request in following the
   * driver's protocol to read data values.
   * @see com.lerchBatesLtd.adMonitorDriver.identify.BAdMonitorDriverReadParams#direction
   */
  public void setDirection(String v) { setString(direction,v,null); }

////////////////////////////////////////////////////////////////
// Type
////////////////////////////////////////////////////////////////
  
  public Type getType() { return TYPE; }
  public static final Type TYPE = Sys.loadType(BAdMonitorDriverReadParams.class);

/*+ ------------ END BAJA AUTO GENERATED CODE -------------- +*/

  public Type getReadRequestType(){return BAdMonitorDriverReadRequest.TYPE;}

    

}