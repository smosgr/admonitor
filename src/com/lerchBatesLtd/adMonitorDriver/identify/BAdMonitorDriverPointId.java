/*
 * Copyright 2014 Lerch Bates Ltd, All Rights Reserved.
 */
package com.lerchBatesLtd.adMonitorDriver.identify;

import javax.baja.sys.*;

import com.tridium.ddf.identify.*;

public class BAdMonitorDriverPointId
  extends BDdfIdParams
{
  /*-
  class BAdMonitorDriverPointId
  {
    properties
    {
      offset : int
        -- This property has nothing to do with the dev
        -- driver framework itself. Instead, we need to
        -- know the location of a point's value when in
        -- the parseReadValue method of AdMonitorDriver's
        -- read response
        default{[0]}
        slotfacets{[MGR_INCLUDE]}
    }
  }
  -*/
/*+ ------------ BEGIN BAJA AUTO GENERATED CODE ------------ +*/
/*@ $com.lerchBatesLtd.adMonitorDriver.identify.BAdMonitorDriverPointId(3564272779)1.0$ @*/
/* Generated Tue Jan 21 13:08:13 GMT 2014 by Slot-o-Matic 2000 (c) Tridium, Inc. 2000 */

////////////////////////////////////////////////////////////////
// Property "offset"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the <code>offset</code> property.
   * This property has nothing to do with the dev driver
   * framework itself. Instead, we need to know the location
   * of a point's value when in the parseReadValue method
   * of AdMonitorDriver's read response
   * @see com.lerchBatesLtd.adMonitorDriver.identify.BAdMonitorDriverPointId#getOffset
   * @see com.lerchBatesLtd.adMonitorDriver.identify.BAdMonitorDriverPointId#setOffset
   */
  public static final Property offset = newProperty(0, 0,MGR_INCLUDE);
  
  /**
   * Get the <code>offset</code> property.
   * This property has nothing to do with the dev driver
   * framework itself. Instead, we need to know the location
   * of a point's value when in the parseReadValue method
   * of AdMonitorDriver's read response
   * @see com.lerchBatesLtd.adMonitorDriver.identify.BAdMonitorDriverPointId#offset
   */
  public int getOffset() { return getInt(offset); }
  
  /**
   * Set the <code>offset</code> property.
   * This property has nothing to do with the dev driver
   * framework itself. Instead, we need to know the location
   * of a point's value when in the parseReadValue method
   * of AdMonitorDriver's read response
   * @see com.lerchBatesLtd.adMonitorDriver.identify.BAdMonitorDriverPointId#offset
   */
  public void setOffset(int v) { setInt(offset,v,null); }

////////////////////////////////////////////////////////////////
// Type
////////////////////////////////////////////////////////////////
  
  public Type getType() { return TYPE; }
  public static final Type TYPE = Sys.loadType(BAdMonitorDriverPointId.class);

/*+ ------------ END BAJA AUTO GENERATED CODE -------------- +*/


}