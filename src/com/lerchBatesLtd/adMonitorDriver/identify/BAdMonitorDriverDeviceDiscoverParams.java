/*
 * Copyright 2014 Lerch Bates Ltd, All Rights Reserved.
 */

package com.lerchBatesLtd.adMonitorDriver.identify;

import javax.baja.sys.*;

import com.tridium.ddf.*;
import com.tridium.ddf.identify.*;

import com.lerchBatesLtd.adMonitorDriver.comm.req.*;
import com.lerchBatesLtd.adMonitorDriver.discover.*;

/**
 * This Niagara AX type needs some properties to represent the
 * data that is necessary in order to construct the byte array
 * for a particular device discovery request.
 */
public class BAdMonitorDriverDeviceDiscoverParams
  extends BDdfDiscoverParams
{
  // Hypothetical protocol example where a gateway can report about
  // devices that are connected to it based on a group number. For
  // example, in the hypothetical protocol, there could be many
  // devices per group. 
  public static final int MIN_GROUP_ID = 0;  // Hypothetical min group id = 0
  public static final int MAX_GROUP_ID = 50; // Hypothetical max group id = 50
  
/*-
  class BAdMonitorDriverDeviceDiscoverParams
  {
    properties
    {
      groupNumber : int
        -- This would work in a hypothetical protocol where a gateway
        -- is capable of reporting devices by group.
        default{[0]}
        slotfacets{[DdfFacets.combine(MGR_INCLUDE,
                    BFacets.make( BFacets.MIN, BInteger.make(MIN_GROUP_ID) ),
                    BFacets.make( BFacets.MAX, BInteger.make(MAX_GROUP_ID)) ) ]}
    
    }
  }
  -*/
/*+ ------------ BEGIN BAJA AUTO GENERATED CODE ------------ +*/
/*@ $com.lerchBatesLtd.adMonitorDriver.identify.BAdMonitorDriverDeviceDiscoverParams(4031774247)1.0$ @*/
/* Generated Tue Jan 21 13:08:13 GMT 2014 by Slot-o-Matic 2000 (c) Tridium, Inc. 2000 */

////////////////////////////////////////////////////////////////
// Property "groupNumber"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the <code>groupNumber</code> property.
   * This would work in a hypothetical protocol where a
   * gateway is capable of reporting devices by group.
   * @see com.lerchBatesLtd.adMonitorDriver.identify.BAdMonitorDriverDeviceDiscoverParams#getGroupNumber
   * @see com.lerchBatesLtd.adMonitorDriver.identify.BAdMonitorDriverDeviceDiscoverParams#setGroupNumber
   */
  public static final Property groupNumber = newProperty(0, 0,DdfFacets.combine(MGR_INCLUDE,
                    BFacets.make( BFacets.MIN, BInteger.make(MIN_GROUP_ID) ),
                    BFacets.make( BFacets.MAX, BInteger.make(MAX_GROUP_ID)) ));
  
  /**
   * Get the <code>groupNumber</code> property.
   * This would work in a hypothetical protocol where a
   * gateway is capable of reporting devices by group.
   * @see com.lerchBatesLtd.adMonitorDriver.identify.BAdMonitorDriverDeviceDiscoverParams#groupNumber
   */
  public int getGroupNumber() { return getInt(groupNumber); }
  
  /**
   * Set the <code>groupNumber</code> property.
   * This would work in a hypothetical protocol where a
   * gateway is capable of reporting devices by group.
   * @see com.lerchBatesLtd.adMonitorDriver.identify.BAdMonitorDriverDeviceDiscoverParams#groupNumber
   */
  public void setGroupNumber(int v) { setInt(groupNumber,v,null); }

////////////////////////////////////////////////////////////////
// Type
////////////////////////////////////////////////////////////////
  
  public Type getType() { return TYPE; }
  public static final Type TYPE = Sys.loadType(BAdMonitorDriverDeviceDiscoverParams.class);

/*+ ------------ END BAJA AUTO GENERATED CODE -------------- +*/

  /**
   * Niagara AX requires a public, empty constructor, so that it can perform
   * Its own introspection operations.
   */
  public BAdMonitorDriverDeviceDiscoverParams()
  {
    
  }
  
  /**
   * Full specified constructor
   */
  public BAdMonitorDriverDeviceDiscoverParams(int groupNo)
  {
    setGroupNumber(groupNo);
  }
  

  public BIDdfDiscoverParams getFirst()
  {
    // TODO: Return an instance of this class that encapsulates the data
    // that would be transmitted as the byte array for the device
    // discovery request in order to request the first known device or
    // group of devices on the network.
    
    // NOTE: The following auto-generated return would work for the
    // hypothetical example that this auto-generated class illustrates.
    return new BAdMonitorDriverDeviceDiscoverParams(MIN_GROUP_ID); // TODO:
  }

  public BIDdfDiscoverParams getLast()
  {
    // TODO: Return an instance of this class that encapsulates the data
    // that would be transmitted as the byte array for the device
    // discovery request in order to request the last known device or
    // group of devices on the network.
    
    // NOTE: The following auto-generated return would work for the
    // hypothetical example that this auto-generated class illustrates.
    return new BAdMonitorDriverDeviceDiscoverParams(MAX_GROUP_ID); // TODO:
  }

  public BIDdfDiscoverParams getNext()
  {
    // TODO: Analyze the current instance of this class and return another
    // instance of this class that encapsulates the data that would be
    // transmitted as the byte array for the device discovery request
    // in order to request the next known device or group of devices
    // on the network.  Good luck!
    
    // NOTE: The following auto-generated return would work for the
    // hypothetical example that this auto-generated class illustrates.
    int nextGroup = getGroupNumber() + 1;
    
    if (nextGroup > MAX_GROUP_ID)
      nextGroup = MAX_GROUP_ID;
    
    return new BAdMonitorDriverDeviceDiscoverParams(nextGroup); // TODO:
  }

  public boolean isAfter(BIDdfDiscoverParams anotherId)
  {
    // TODO: Analyze the current instance as well as the given instance
    // of this class. Return true if the current instance of this class
    // encapsulates data that would be transmitted as the byte array
    // for a device discovery request that would request a device or
    // group of devices that is after those which the given instance's
    // encapsulated data would request.
    
    // NOTE: The following auto-generated/ return would work for the
    // hypothetical example that this auto- generated class illustrates.
    BAdMonitorDriverDeviceDiscoverParams otherDeviceId = 
      (BAdMonitorDriverDeviceDiscoverParams)anotherId;
    
    return this.getGroupNumber() > otherDeviceId.getGroupNumber(); // TODO:
  }


  public Type getDiscoverRequestType() 
  { 
    return BAdMonitorDriverDeviceDiscoverRequest.TYPE; 
  }
  
  /**
   * This tells the developer driver framework that
   * instances of BYourDriverDiscoveryLeaf will be
   * placed into the discovery list of the point
   * manager to represent each data point that the
   * driver discovers.
   */
  public Type getDiscoveryLeafType()
  { 
    // NOTE: The deviceId can usually serve as the discovery leaf during the
    // device discovery process.
    return BAdMonitorDriverDeviceId.TYPE;
  }
  
}