/*
 * Copyright 2014 Lerch Bates Ltd, All Rights Reserved.
 */
package com.lerchBatesLtd.adMonitorDriver.identify;

import javax.baja.sys.*;

import com.tridium.ddf.identify.*;

import com.lerchBatesLtd.adMonitorDriver.comm.req.*;
import com.lerchBatesLtd.adMonitorDriver.identify.*;

public class BAdMonitorDriverWriteParams
  extends BDdfIdParams
  implements BIDdfWriteParams
{
  /*-
  class BAdMonitorDriverWriteParams
  {
    properties
    {
      forceWrite : boolean
        -- This property has nothing to with the dev
        -- driver framework itself. Instead, we need
        -- to construct the toByteArray method of the
        -- driver's write request in following the
        -- driver's protocol to write data values.
        -- In this hypothetical protocol, if we do not
        -- forceWrite then the equipment's internal
        -- program could overwrite any change that
        -- Niagara might make to a data value.
        default{[true]}
    }
  }
  -*/
/*+ ------------ BEGIN BAJA AUTO GENERATED CODE ------------ +*/
/*@ $com.lerchBatesLtd.adMonitorDriver.identify.BAdMonitorDriverWriteParams(3970858911)1.0$ @*/
/* Generated Tue Jan 21 13:08:13 GMT 2014 by Slot-o-Matic 2000 (c) Tridium, Inc. 2000 */

////////////////////////////////////////////////////////////////
// Property "forceWrite"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the <code>forceWrite</code> property.
   * This property has nothing to with the dev driver framework
   * itself. Instead, we need to construct the toByteArray
   * method of the driver's write request in following the driver's protocol to write data values. In this hypothetical protocol, if we do not forceWrite then the equipment's internal program could overwrite any change that Niagara might make to a data value.
   * @see com.lerchBatesLtd.adMonitorDriver.identify.BAdMonitorDriverWriteParams#getForceWrite
   * @see com.lerchBatesLtd.adMonitorDriver.identify.BAdMonitorDriverWriteParams#setForceWrite
   */
  public static final Property forceWrite = newProperty(0, true,null);
  
  /**
   * Get the <code>forceWrite</code> property.
   * This property has nothing to with the dev driver framework
   * itself. Instead, we need to construct the toByteArray
   * method of the driver's write request in following the driver's protocol to write data values. In this hypothetical protocol, if we do not forceWrite then the equipment's internal program could overwrite any change that Niagara might make to a data value.
   * @see com.lerchBatesLtd.adMonitorDriver.identify.BAdMonitorDriverWriteParams#forceWrite
   */
  public boolean getForceWrite() { return getBoolean(forceWrite); }
  
  /**
   * Set the <code>forceWrite</code> property.
   * This property has nothing to with the dev driver framework
   * itself. Instead, we need to construct the toByteArray
   * method of the driver's write request in following the driver's protocol to write data values. In this hypothetical protocol, if we do not forceWrite then the equipment's internal program could overwrite any change that Niagara might make to a data value.
   * @see com.lerchBatesLtd.adMonitorDriver.identify.BAdMonitorDriverWriteParams#forceWrite
   */
  public void setForceWrite(boolean v) { setBoolean(forceWrite,v,null); }

////////////////////////////////////////////////////////////////
// Type
////////////////////////////////////////////////////////////////
  
  public Type getType() { return TYPE; }
  public static final Type TYPE = Sys.loadType(BAdMonitorDriverWriteParams.class);

/*+ ------------ END BAJA AUTO GENERATED CODE -------------- +*/

  public Type getWriteRequestType(){return BAdMonitorDriverWriteRequest.TYPE;}
  }