/*
 * Copyright 2014 Lerch Bates Ltd, All Rights Reserved.
 */

package com.lerchBatesLtd.adMonitorDriver.identify;

import javax.baja.registry.TypeInfo;
import javax.baja.sys.Property;
import javax.baja.sys.Sys;
import javax.baja.sys.Type;

import com.lerchBatesLtd.adMonitorDriver.BAdMonitorDriverDevice;
import com.lerchBatesLtd.adMonitorDriver.comm.req.BAdMonitorDriverPingRequest;
import com.tridium.ddf.discover.BIDdfDiscoveryLeaf;
import com.tridium.ddf.identify.BDdfDeviceId;

public class BAdMonitorDriverDeviceId
  extends BDdfDeviceId
      implements BIDdfDiscoveryLeaf
  
{
  /*-
  class BAdMonitorDriverDeviceId
  {
    properties
    {
      controllerId : int
        -- This is the unitNumber in our hypothetical protocol.
        default{[0]}
        slotfacets{[MGR_INCLUDE]}
    }
  }
  -*/
/*+ ------------ BEGIN BAJA AUTO GENERATED CODE ------------ +*/
/*@ $com.lerchBatesLtd.adMonitorDriver.identify.BAdMonitorDriverDeviceId(3852840126)1.0$ @*/
/* Generated Tue Jan 21 13:10:24 GMT 2014 by Slot-o-Matic 2000 (c) Tridium, Inc. 2000 */

////////////////////////////////////////////////////////////////
// Property "controllerId"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the <code>controllerId</code> property.
   * This is the unitNumber in our hypothetical protocol.
   * @see com.lerchBatesLtd.adMonitorDriver.identify.BAdMonitorDriverDeviceId#getControllerId
   * @see com.lerchBatesLtd.adMonitorDriver.identify.BAdMonitorDriverDeviceId#setControllerId
   */
  public static final Property controllerId = newProperty(0, 0,MGR_INCLUDE);
  
  /**
   * Get the <code>controllerId</code> property.
   * This is the unitNumber in our hypothetical protocol.
   * @see com.lerchBatesLtd.adMonitorDriver.identify.BAdMonitorDriverDeviceId#controllerId
   */
  public int getControllerId() { return getInt(controllerId); }
  
  /**
   * Set the <code>controllerId</code> property.
   * This is the unitNumber in our hypothetical protocol.
   * @see com.lerchBatesLtd.adMonitorDriver.identify.BAdMonitorDriverDeviceId#controllerId
   */
  public void setControllerId(int v) { setInt(controllerId,v,null); }

////////////////////////////////////////////////////////////////
// Type
////////////////////////////////////////////////////////////////
  
  public Type getType() { return TYPE; }
  public static final Type TYPE = Sys.loadType(BAdMonitorDriverDeviceId.class);

/*+ ------------ END BAJA AUTO GENERATED CODE -------------- +*/

  public Type getPingRequestType( )
  {
    return BAdMonitorDriverPingRequest.TYPE;
  }

  /**
   * Niagara AX requires a public, empty constructor, so that it can perform
   * Its own introspection operations.
   */
  public BAdMonitorDriverDeviceId(){}
  
  
    
  /**
   * When a control point is added to the station from the Dev
   * Point Manager, it is given this name by default (possibly
   * with a suffix to make it unique).
   * @return
   */
  public String getDiscoveryName()
  {
    return "NewDevice";
  } 
  
  /**
   * Descendants need to return an array of TypeInfo objects corresponding
   * to all valid Niagara Ax types for this discovery object. This is
   * important when the end-user clicks 'Add' from the user interface for
   * the manager.
   *
   * For this discovery object, please  return a list of the types
   * which may be used to model it as a BComponent in the station
   * database. If the discovery object represents a device in your
   * driver then then method should return an array with size of
   * at least one, filled with TypeInfo's that wrap the Niagara AX
   * TYPE's for your driver's device components.
   *
   * The type at index 0 in the array should be the type which
   * provides the best mapping.  Please return an empty array if the
   * discovery cannot be mapped.
   */
  public TypeInfo[] getValidDatabaseTypes()
  {
    return new TypeInfo[]{BAdMonitorDriverDevice.TYPE.getTypeInfo()};
  }  
  
////////////////////////////////////////////////////////////////
// Attributes
////////////////////////////////////////////////////////////////
  
  private static final int FIRST_UNIT_NBR = 0;
  private static final int LAST_UNIT_NBR = 50;
  
}