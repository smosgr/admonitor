/*
 * Copyright 2014 Lerch Bates Ltd, All Rights Reserved.
 */

package com.lerchBatesLtd.adMonitorDriver;

import javax.baja.sys.*;

import com.tridium.ddf.*;

public class BAdMonitorDriverDeviceFolder
  extends BDdfDeviceFolder
{
  /*-
  class BAdMonitorDriverDeviceFolder
  {
    properties
    {
    }
  }
  -*/
/*+ ------------ BEGIN BAJA AUTO GENERATED CODE ------------ +*/
/*@ $com.lerchBatesLtd.adMonitorDriver.BAdMonitorDriverDeviceFolder(3644648008)1.0$ @*/
/* Generated Tue Jan 21 13:08:13 GMT 2014 by Slot-o-Matic 2000 (c) Tridium, Inc. 2000 */

////////////////////////////////////////////////////////////////
// Type
////////////////////////////////////////////////////////////////
  
  public Type getType() { return TYPE; }
  public static final Type TYPE = Sys.loadType(BAdMonitorDriverDeviceFolder.class);

/*+ ------------ END BAJA AUTO GENERATED CODE -------------- +*/
}