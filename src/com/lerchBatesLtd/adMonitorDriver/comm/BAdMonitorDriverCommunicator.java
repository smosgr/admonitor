/*
 * Copyright 2014 Lerch Bates Ltd, All Rights Reserved.
 */

package com.lerchBatesLtd.adMonitorDriver.comm;

import javax.baja.sys.Property;
import javax.baja.sys.Sys;
import javax.baja.sys.Type;

import com.tridium.ddf.comm.defaultComm.BDdfReceiver;

import com.tridium.ddfSerial.comm.singleTransaction.*;

public class BAdMonitorDriverCommunicator  extends BDdfSerialSitCommunicator
{
  /*-
  class BAdMonitorDriverCommunicator
  {
    properties
    {
    
      receiver : BDdfReceiver
        -- Plugs yourDriver's custom receiver
        -- into yourDriver's communicator
        default{[new BAdMonitorDriverReceiver()]}
    
    }
  }
  -*/
/*+ ------------ BEGIN BAJA AUTO GENERATED CODE ------------ +*/
/*@ $com.lerchBatesLtd.adMonitorDriver.comm.BAdMonitorDriverCommunicator(3198605607)1.0$ @*/
/* Generated Tue Jan 21 13:08:13 GMT 2014 by Slot-o-Matic 2000 (c) Tridium, Inc. 2000 */

////////////////////////////////////////////////////////////////
// Property "receiver"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the <code>receiver</code> property.
   * Plugs yourDriver's custom receiver into yourDriver's
   * communicator
   * @see com.lerchBatesLtd.adMonitorDriver.comm.BAdMonitorDriverCommunicator#getReceiver
   * @see com.lerchBatesLtd.adMonitorDriver.comm.BAdMonitorDriverCommunicator#setReceiver
   */
  public static final Property receiver = newProperty(0, new BAdMonitorDriverReceiver(),null);
  
  /**
   * Get the <code>receiver</code> property.
   * Plugs yourDriver's custom receiver into yourDriver's
   * communicator
   * @see com.lerchBatesLtd.adMonitorDriver.comm.BAdMonitorDriverCommunicator#receiver
   */
  public BDdfReceiver getReceiver() { return (BDdfReceiver)get(receiver); }
  
  /**
   * Set the <code>receiver</code> property.
   * Plugs yourDriver's custom receiver into yourDriver's
   * communicator
   * @see com.lerchBatesLtd.adMonitorDriver.comm.BAdMonitorDriverCommunicator#receiver
   */
  public void setReceiver(BDdfReceiver v) { set(receiver,v,null); }

////////////////////////////////////////////////////////////////
// Type
////////////////////////////////////////////////////////////////
  
  public Type getType() { return TYPE; }
  public static final Type TYPE = Sys.loadType(BAdMonitorDriverCommunicator.class);

/*+ ------------ END BAJA AUTO GENERATED CODE -------------- +*/


}