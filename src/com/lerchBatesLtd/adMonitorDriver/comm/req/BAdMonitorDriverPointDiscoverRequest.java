/*
 * Copyright 2014 Lerch Bates Ltd, All Rights Reserved.
 */
package com.lerchBatesLtd.adMonitorDriver.comm.req;

import javax.baja.sys.*;

import com.tridium.ddf.identify.*;
import com.tridium.ddf.comm.*;
import com.tridium.ddf.comm.req.*;
import com.tridium.ddf.comm.rsp.*;
import com.lerchBatesLtd.adMonitorDriver.identify.*;
import com.lerchBatesLtd.adMonitorDriver.comm.rsp.*;


public class BAdMonitorDriverPointDiscoverRequest
  extends BDdfDiscoveryRequest
{
  /*-
  class BAdMonitorDriverPointDiscoverRequest
  {
  }
  -*/
/*+ ------------ BEGIN BAJA AUTO GENERATED CODE ------------ +*/
/*@ $com.lerchBatesLtd.adMonitorDriver.comm.req.BAdMonitorDriverPointDiscoverRequest(988529787)1.0$ @*/
/* Generated Tue Jan 21 13:08:13 GMT 2014 by Slot-o-Matic 2000 (c) Tridium, Inc. 2000 */

////////////////////////////////////////////////////////////////
// Type
////////////////////////////////////////////////////////////////
  
  public Type getType() { return TYPE; }
  public static final Type TYPE = Sys.loadType(BAdMonitorDriverPointDiscoverRequest.class);

/*+ ------------ END BAJA AUTO GENERATED CODE -------------- +*/

  public byte[] toByteArray()
  {
    // In the developer driver framework, all requests are automatically
    // assigned a deviceId when they are created. The developer driver
    // framework calls this method (function) after it creates the
    // request, therefore this particular request has already been
    // assigned a deviceId. The deviceId will be an instance of
    // BAdMonitorDriverDeviceId - that is how the developer driver works!
   //--- BAdMonitorDriverDeviceId deviceId =
   //---  (BAdMonitorDriverDeviceId)getDeviceId();
    
    // Likewise, all discover requests are automatically assigned a 'discover'
    // parameters structure. The discoverParameters will be an instance of
    // BAdMonitorDriverPointDiscoverParams - that is how the developer driver
    // works! The actual value will  be based on what an end-user selects
    // when he or she clicks the 'Discover' button on the device manager.
   //--- BAdMonitorDriverPointDiscoverParams discoverParams
   //---   = (BAdMonitorDriverPointDiscoverParams)getDiscoverParameters();
    
    // TODO: Construct a byte array or a byte array output stream to
    // construct the byte array that this method will return. Use the
    // data in both the 'deviceId' and the 'discoverParams' to initialize
    // the byte array or byte array output stream. The deviceId should
    // have all data necessary to identify the particular field-device.
    // The discoverParams should have all data necessary to identify which
    // point(s) to seek within the field-device.
    
    // NOTE: This is similar to a read request but could differ in that
    // instead of asking the field-device for the value of one or more
    // data points, this asks the field device about the existence and/or
    // identification of one or more data points. Try to use the data in the
    // discoverRequest as an indication of which particular data point(s)
    // to query the field-device about.
    
    // TODO: Return a byte array...good luck.
    
    
    
    StringBuffer msg = new StringBuffer();
    msg.append((char)0); //start frame
    msg.append("discover ");
    msg.append((char)23); // end frame  ---------- to CHANGE! Theloyme 265
    
    return msg.toString().getBytes();

  }
  
  public BIDdfResponse processReceive(IDdfDataFrame receiveFrame) 
     throws DdfResponseException 
  {  
    // TODO: Pass any data to the response that it will need to 
    // implement its parseDiscoveryChildren method. 
    
    String content = new String (receiveFrame.getFrameBytes(), 0, receiveFrame.getFrameSize()).trim();
    
    if (content.startsWith("discover "))
    {
      if (content.startsWith("discover error"))
        throw new DdfResponseException(content);
      else
      {
        return new BAdMonitorDriverPointDiscoverResponse(receiveFrame);
      }
      
           
    }
    
    
    
    return null; 
  }   
}