/*
 * Copyright 2014 Lerch Bates Ltd, All Rights Reserved.
 */
package com.lerchBatesLtd.adMonitorDriver.comm.req;

import javax.baja.sys.*;
import javax.baja.io.*;

import com.tridium.ddf.comm.*;
import com.tridium.ddf.comm.req.*;
import com.tridium.ddf.comm.rsp.*;


import com.lerchBatesLtd.adMonitorDriver.identify.*;
import com.lerchBatesLtd.adMonitorDriver.comm.rsp.*;
import com.lerchBatesLtd.adMonitorDriver.identify.*;

public class BAdMonitorDriverReadRequest
  extends BDdfReadRequest
  {
  /*-
  class BAdMonitorDriverReadRequest
  {
      }
  -*/
/*+ ------------ BEGIN BAJA AUTO GENERATED CODE ------------ +*/
/*@ $com.lerchBatesLtd.adMonitorDriver.comm.req.BAdMonitorDriverReadRequest(1135946707)1.0$ @*/
/* Generated Tue Jan 21 13:08:13 GMT 2014 by Slot-o-Matic 2000 (c) Tridium, Inc. 2000 */

////////////////////////////////////////////////////////////////
// Type
////////////////////////////////////////////////////////////////
  
  public Type getType() { return TYPE; }
  public static final Type TYPE = Sys.loadType(BAdMonitorDriverReadRequest.class);

/*+ ------------ END BAJA AUTO GENERATED CODE -------------- +*/

  public byte[] toByteArray()
  {
    // In the developer driver framework, all requests are automatically
    // assigned a deviceId when they are created. The developer driver
    // framework calls this method (function) after it creates the
    // request, therefore this particular request has already been
    // assigned a deviceId. The deviceId will be an instance of
    // BAdMonitorDriverDeviceId - that is how the developer driver works!
    BAdMonitorDriverDeviceId deviceId =
      (BAdMonitorDriverDeviceId)getDeviceId();
    
    StringBuffer msg = new StringBuffer();
    msg.append((char)1);
    msg.append("get ");
    msg.append((char)23); //toCHANGE
    return msg.toString().getBytes();
    
    // Likewise, all read requests are automatically assigned a read
    // parameters structure. The readParameters will be an instance of
    // BAdMonitorDriverReadParams - that is how the developer driver
    // works!
   // BAdMonitorDriverReadParams readParams = (BAdMonitorDriverReadParams)getReadParameters();
    
    // TODO: Construct a byte array or a byte array output stream to
    // construct the byte array that this method will return. Use the
    // data in both the 'deviceId' and the 'readParams' to initialize
    // the byte array or byte array output stream. The deviceId should
    // have all data necessary to identify the particular field-device.
    // The readParams should have all data necessary to identify which
    // point(s) to read within the field-device.
    
    // NOTE: All IDdfReadables (driver proxy points) that share the same
    // Read parameters structure will typically be automatically grouped
    // into a single request. Therefore, this request might retrieve
    // the values for multiple proxy points **if** the AdMonitorDriver
    // protocol supports retrieving multiple data point values in a single
    // request-response transaction.
    
    // TODO: Return a byte array...good luck.
   
  }

  /**
   * After transmitting this request, the BDdfCommunicator will pass frames that it receives
   * here. If you implement the getTag method then the ddf communicator will only pass data frames
   * whose tag's hashcode matches your request tag's hashcode. If your request returns null from
   * the getTag method then all received data frames will be passed here, until the request times
   * out or returns a BIDdfResponse from this method.
   *
   *  This request needs to take one of the following steps:
   *   1. NOT TYPICAL: Ignore the frame and return null.
   *   2. NOT TYPICAL: Collect the frame and return a BIDdfMultiFrameResponse. In which case, you need to implement your own collection
   *      mechanism. For example, this could be as simple as putting them all in a Vector in the BIDdfMultiFrameResponse.
   *   3. TYPICAL: Return a BIDdfResponse for the data frame and NOT TYPICAL:> any previously collected frames that you determine together make up a completed response.
   *   4. TYPICAL: Throw an DdfResponseException or subclass there-of to indicate the the frame
   *   forms a complete message but indicates an error condition in the device preventing
   *   a successful response.
   *
   *   WARNING: In scenario's 2 and 3, please copy the frame's bytes as the frame's byte array could be a direct reference to an internal
   *   buffer in the receiver.
   * @param iDdfDataFrame
   * @return
   */
  public BIDdfResponse processReceive(IDdfDataFrame receiveFrame)
    throws DdfResponseException
  {
    
    String content = new String (receiveFrame.getFrameBytes(), 0, receiveFrame.getFrameSize()).trim();
    
    if (content.startsWith("get "))
        {
          if (content.startsWith("get error"))
            throw new DdfResponseException(content);
          else
          {
            return new BAdMonitorDriverReadResponse(receiveFrame,
                                                    (BAdMonitorDriverReadParams)getReadParameters());
            
          }
        }

    return null;
  }
  
  }