/*
 * Copyright 2014 Lerch Bates Ltd, All Rights Reserved.
 */

package com.lerchBatesLtd.adMonitorDriver.comm.req;

import javax.baja.sys.Sys;
import javax.baja.sys.Type;

import com.lerchBatesLtd.adMonitorDriver.comm.rsp.BAdMonitorDriverPingResponse;
import com.tridium.ddf.comm.IDdfDataFrame;
import com.tridium.ddf.comm.req.BDdfPingRequest;
import com.tridium.ddf.comm.rsp.BIDdfResponse;
import com.tridium.ddf.comm.rsp.DdfResponseException;

public class BAdMonitorDriverPingRequest
  extends BDdfPingRequest
  {
   
  
    /*- 
  class BAdMonitorDriverPingRequest 
  { 
  } 
  -*/
/*+ ------------ BEGIN BAJA AUTO GENERATED CODE ------------ +*/
/*@ $com.lerchBatesLtd.adMonitorDriver.comm.req.BAdMonitorDriverPingRequest(2543209779)1.0$ @*/
/* Generated Tue Jan 21 13:08:13 GMT 2014 by Slot-o-Matic 2000 (c) Tridium, Inc. 2000 */

////////////////////////////////////////////////////////////////
// Type
////////////////////////////////////////////////////////////////
  
  public Type getType() { return TYPE; }
  public static final Type TYPE = Sys.loadType(BAdMonitorDriverPingRequest.class);

/*+ ------------ END BAJA AUTO GENERATED CODE -------------- +*/ 
    
  

  public byte[] toByteArray()
  {
    // In the developer driver framework, all requests are automatically
    // Assigned a deviceId when they are created. The developer driver
    // Framework calls this method (function) after it creates the
    // Request, therefore this particular request has already been
    // Assigned a deviceId. The deviceId will be an instance of
    // BYourDriverDeviceId - that is how the developer driver works!

    //BAdMonitorDriverDeviceId deviceId =
      //(BAdMonitorDriverDeviceId)getDeviceId();
    
    final byte SOH = 0x01;
    final byte EOT = 0X17; //decimal 23          //(byte) 0xFF;--> decimal 255
    
    
    StringBuffer msg = new StringBuffer();
    msg.append(SOH); //start frame
    msg.append("ping");  // content
    msg.append(EOT); // end of frame
    return msg.toString().getBytes();
  }

  /**
   * For this example, we will assume that the mere fact that a data
   * frame was received after transmitting this response means that
   * the equipment must have responded to the request. Since in
   * Niagara AX, the primary purpose of a ping request-response
   * transaction is to determine whether or not the corresponding
   * field-device is online, then this will suffice.
   */
  public BIDdfResponse processReceive(IDdfDataFrame receiveFrame) throws DdfResponseException
  {
    // TODO Auto-generated method stub
    
    
    //get the response as a string
    String response = new String(receiveFrame.getFrameBytes(), 1, receiveFrame.getFrameSize()).trim();
    
    //is this a ping response?
    if (response.startsWith("ping "))
    {
      if (response.equals("ping ok!"))
            return new BAdMonitorDriverPingResponse();
    else
      throw new DdfResponseException("ping failed: "+ response);
    }else    
    return null;
  }
  
  
}