/*
 * Copyright 2014 Lerch Bates Ltd, All Rights Reserved.
 */

package com.lerchBatesLtd.adMonitorDriver.comm.req; 
 
import javax.baja.sys.*;

import com.tridium.ddf.comm.IDdfDataFrame;
import com.tridium.ddf.comm.req.*; 
import com.tridium.ddf.comm.rsp.BIDdfResponse;
import com.tridium.ddf.comm.rsp.DdfResponseException;

import com.lerchBatesLtd.adMonitorDriver.comm.rsp.*;
import com.lerchBatesLtd.adMonitorDriver.identify.*;
 
public class BAdMonitorDriverDeviceDiscoverRequest 
  extends BDdfDiscoveryRequest 
{ 
  /*- 
  class BAdMonitorDriverDeviceDiscoverRequest 
  { 
  } 
  -*/
/*+ ------------ BEGIN BAJA AUTO GENERATED CODE ------------ +*/
/*@ $com.lerchBatesLtd.adMonitorDriver.comm.req.BAdMonitorDriverDeviceDiscoverRequest(3124418724)1.0$ @*/
/* Generated Tue Jan 21 13:08:13 GMT 2014 by Slot-o-Matic 2000 (c) Tridium, Inc. 2000 */

////////////////////////////////////////////////////////////////
// Type
////////////////////////////////////////////////////////////////
  
  public Type getType() { return TYPE; }
  public static final Type TYPE = Sys.loadType(BAdMonitorDriverDeviceDiscoverRequest.class);

/*+ ------------ END BAJA AUTO GENERATED CODE -------------- +*/ 
 
  public byte[] toByteArray() 
  { 
    
    // All discover requests are automatically assigned a 'discover'
    // parameters structure. The discoverParameters will be an instance of
    // BAdMonitorDriverDeviceDiscoverParams - that is how the developer driver
    // works! The actual value will be based on what an end-user selects
    // when he or she clicks the 'Discover' button on the device manager.
    BAdMonitorDriverDeviceDiscoverParams deviceDscvParams = 
      (BAdMonitorDriverDeviceDiscoverParams)getDiscoverParameters(); 

    
    // TODO: Construct a byte array or a byte array output stream to
    // construct the byte array that this method will return. Use the
    // data in the 'discoverParams' to initialize the byte array or
    // byte array output stream.  The deviceDscvParams should have all
    // data necessary to identify which devices to seek from the field
    // bus. 
    
    // NOTE: This is similar to a ping request but could differ in that
    // instead of asking the field-device if it is online, this asks the
    // trunk to report back whether or not a device or range of devices
    // exists. Some protocols might have a special gateway field-device
    // reply to the request whereas other protocols might have all devices
    // reply but have them reply using a timing sequence so as to avoid
    // collisions during network data transfer.
    
    // TODO: Return a byte array...good luck.
    return null;
    
  }
  
  public BIDdfResponse processReceive(IDdfDataFrame receiveFrame)
    throws DdfResponseException
  {
    // TODO: Pass any data to the response that it will need to 
    // implement its parseDiscoveryChildren method. 
    return new BAdMonitorDriverDeviceDiscoverResponse(receiveFrame); 
  }
}