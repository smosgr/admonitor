/*
 * Copyright 2014 Lerch Bates Ltd, All Rights Reserved.
 */
package com.lerchBatesLtd.adMonitorDriver.comm.rsp; 
 
import java.util.*; 

import javax.baja.sys.*;
import javax.baja.util.*;

import com.tridium.ddf.comm.*; 
import com.tridium.ddf.comm.rsp.*; 
import com.tridium.ddf.discover.*;

import com.lerchBatesLtd.adMonitorDriver.discover.*; 
import com.lerchBatesLtd.adMonitorDriver.identify.*; 
 
public class BAdMonitorDriverPointDiscoverResponse 
  extends BDdfResponse 
  implements BIDdfDiscoverResponse 
{ 
  /*- 
  class BAdMonitorDriverPointDiscoverResponse 
  { 
  } 
  -*/
/*+ ------------ BEGIN BAJA AUTO GENERATED CODE ------------ +*/
/*@ $com.lerchBatesLtd.adMonitorDriver.comm.rsp.BAdMonitorDriverPointDiscoverResponse(1180952328)1.0$ @*/
/* Generated Tue Jan 21 13:08:13 GMT 2014 by Slot-o-Matic 2000 (c) Tridium, Inc. 2000 */

////////////////////////////////////////////////////////////////
// Type
////////////////////////////////////////////////////////////////
  
  public Type getType() { return TYPE; }
  public static final Type TYPE = Sys.loadType(BAdMonitorDriverPointDiscoverResponse.class);

/*+ ------------ END BAJA AUTO GENERATED CODE -------------- +*/ 

  
  private byte[] receiveBytes;
  
  public BAdMonitorDriverPointDiscoverResponse() 
  { 
  }
  
  /** 
   * This constructor does not necessarily need to take an IDdfDataFrame 
   * as a parameter. It could take any parameters that you wish to pass 
   * to the response from the request's processReceive method. The data 
   * that is passed to this constructor should be saved on instance variables 
   * and used in the parseDiscoveryObjects method to construct the return 
   * array. 
   */ 
  public BAdMonitorDriverPointDiscoverResponse(IDdfDataFrame receiveFrame) 
  { 
    // TODO: Make a copy of any bytes that you need from the receiveFrame 
    //       since the receive frame could be part of an internal buffer 
    //       of the receiver.
    
    // SUGGESTION: Plan on using This 'cached' data in the parseDiscoveryObjects
    // method below.
    
    
    receiveBytes = new byte[receiveFrame.getFrameSize()];
    System.arraycopy(receiveFrame.getFrameBytes(), 0, receiveBytes, 0, receiveFrame.getFrameSize());
  }
  
  /** 
   * This method parses the response byte array and returns an 
   * array of BAdMonitorDriverPointDiscoveryLeaf objects describing 
   * the data points that this response is able to identify. 
   * This is called during the auto discovery process. 
   */ 
  public BIDdfDiscoveryObject[] parseDiscoveryObjects(Context c) 
  { 
    // TODO: Convert the 'cached' data into an array of 
    // BAdMonitorDriverPointDiscoveryLeaf objects. Each particular
    // discovery leaf object should identify one atomic data
    // point in the field-device. An atomic data point is one that
    // can be represented in Niagara as a BStringPoint, BNumericPoint,
    // BEnumPoint, or BBooleanPoint. Each object in the array
    // represents an atomic data point available in or accessible by
    // the field-device. A row will automatically appear in the point
    // manager for each object in the array that is returned. Good luck.
    
    
    BAdMonitorDriverDeviceId deviceId = (BAdMonitorDriverDeviceId) getDeviceId();
    
    Array list = new Array (BIDdfDiscoveryObject.class);
    String content = new String(receiveBytes).trim();
    content = content.substring("discover ".length());
    
    StringTokenizer controllers = new StringTokenizer(content, "\t");
    
    
    while(controllers.hasMoreTokens())
    {
      String id = controllers.nextToken();
      String controllerId = id.substring(0, id.indexOf(' '));
      
      String unitId = id.substring(id.indexOf(' ') + 1, id.length());
      
      
      
      if (controllerId.equals(deviceId.getControllerId()))
          {
            BAdMonitorDriverPointDiscoveryLeaf leaf = new BAdMonitorDriverPointDiscoveryLeaf();
            BAdMonitorDriverPointId pointId = new BAdMonitorDriverPointId();
            
          //  pointId.setOffset(unitId); // toCHANGE!!!
        
            leaf.setPointId(pointId);
            list.add(leaf);
          }
      
    }
    
    return (BIDdfDiscoveryObject[]) list.trim();
  }

}