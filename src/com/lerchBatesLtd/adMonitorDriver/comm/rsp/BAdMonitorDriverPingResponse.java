/*
 * Copyright 2014 Lerch Bates Ltd, All Rights Reserved.
 */

package com.lerchBatesLtd.adMonitorDriver.comm.rsp;

import javax.baja.sys.*;

import com.tridium.ddf.comm.rsp.*;

import com.lerchBatesLtd.adMonitorDriver.identify.*;

public class BAdMonitorDriverPingResponse
  extends BDdfResponse
  {
  /*-
  class BAdMonitorDriverPingResponse
  {
  }
  -*/
/*+ ------------ BEGIN BAJA AUTO GENERATED CODE ------------ +*/
/*@ $com.lerchBatesLtd.adMonitorDriver.comm.rsp.BAdMonitorDriverPingResponse(151115272)1.0$ @*/
/* Generated Tue Jan 21 13:08:13 GMT 2014 by Slot-o-Matic 2000 (c) Tridium, Inc. 2000 */

////////////////////////////////////////////////////////////////
// Type
////////////////////////////////////////////////////////////////
  
  public Type getType() { return TYPE; }
  public static final Type TYPE = Sys.loadType(BAdMonitorDriverPingResponse.class);

/*+ ------------ END BAJA AUTO GENERATED CODE -------------- +*/

   
        

}