/*
 * Copyright 2014 Lerch Bates Ltd, All Rights Reserved.
 */
package com.lerchBatesLtd.adMonitorDriver.comm.rsp; 
 
import javax.baja.sys.*;

import com.tridium.ddf.comm.*; 
import com.tridium.ddf.comm.rsp.*;

import com.tridium.ddf.discover.BIDdfDiscoveryObject;
 
public class BAdMonitorDriverDeviceDiscoverResponse 
  extends BDdfResponse 
  implements BIDdfDiscoverResponse 
{ 
  /*- 
  class BAdMonitorDriverDeviceDiscoverResponse 
  { 
  } 
  -*/
/*+ ------------ BEGIN BAJA AUTO GENERATED CODE ------------ +*/
/*@ $com.lerchBatesLtd.adMonitorDriver.comm.rsp.BAdMonitorDriverDeviceDiscoverResponse(1760894632)1.0$ @*/
/* Generated Tue Jan 21 13:08:13 GMT 2014 by Slot-o-Matic 2000 (c) Tridium, Inc. 2000 */

////////////////////////////////////////////////////////////////
// Type
////////////////////////////////////////////////////////////////
  
  public Type getType() { return TYPE; }
  public static final Type TYPE = Sys.loadType(BAdMonitorDriverDeviceDiscoverResponse.class);

/*+ ------------ END BAJA AUTO GENERATED CODE -------------- +*/
  
  public BAdMonitorDriverDeviceDiscoverResponse() 
  { 
  }
  
  /** 
   * This constructor does not necessarily need to take an IDdfDataFrame 
   * as a parameter. It could take any parameters that you wish to pass 
   * to the response from the request's processReceive method. The data 
   * that is passed to this constructor should be saved on instance variables 
   * and used in the parseDiscoveryObjects method to construct the return 
   * array. 
   */ 
  public BAdMonitorDriverDeviceDiscoverResponse(IDdfDataFrame receiveFrame) 
  { 
    // TODO: Make a copy of any bytes that you need from the receiveFrame 
    //       since the receive frame could be part of an internal buffer 
    //       of the receiver.
    
    // SUGGESTION: Plan on using This 'cached' data in the parseDiscoveryObjects
    // method below.
  }
   
   /** 
    * This method parses the response byte array and returns an 
    * array of discovery objects describing the devices  that
    * this response is able to identify. This is called during 
    * the auto discovery process.
    * 
    * In the simplest of scenarios, this may return an array of
    * BAdMonitorDriverDeviceId objects representing the field-devices
    * that are known on the network. 
    */ 
   public BIDdfDiscoveryObject[] parseDiscoveryObjects(Context c) 
   { 
     // TODO: Convert the 'cached' data from the constructor into an array of 
     // discovery objects. Each particular discovery leaf object should
     // identify one field-device that is on the trunk. in the simplest of
     // scenarios, this may return an array of BAdMonitorDriverDeviceId objects
     // to represent the field-devices that are known on the network.
     return null;
   }
}