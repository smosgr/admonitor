/*
 * Copyright 2014 Lerch Bates Ltd, All Rights Reserved.
 */
package com.lerchBatesLtd.adMonitorDriver.comm.rsp;

import java.util.*;
import javax.baja.control.*;
import javax.baja.sys.*;
import javax.baja.status.*;


import com.tridium.ddf.comm.*;
import com.tridium.ddf.comm.IDdfDataFrame;
import com.tridium.ddf.comm.req.IDdfReadable;
import com.tridium.ddf.comm.rsp.*;
import com.tridium.ddf.comm.req.*;

import com.lerchBatesLtd.adMonitorDriver.identify.*;
import com.lerchBatesLtd.adMonitorDriver.point.*;

public class BAdMonitorDriverReadResponse
  extends BDdfResponse
  implements BIDdfReadResponse {
  /*-
  class BAdMonitorDriverReadResponse
  {
  }
  -*/
/*+ ------------ BEGIN BAJA AUTO GENERATED CODE ------------ +*/
/*@ $com.lerchBatesLtd.adMonitorDriver.comm.rsp.BAdMonitorDriverReadResponse(1577940703)1.0$ @*/
/* Generated Tue Jan 21 13:08:13 GMT 2014 by Slot-o-Matic 2000 (c) Tridium, Inc. 2000 */

////////////////////////////////////////////////////////////////
// Type
////////////////////////////////////////////////////////////////
  
  public Type getType() { return TYPE; }
  public static final Type TYPE = Sys.loadType(BAdMonitorDriverReadResponse.class);

/*+ ------------ END BAJA AUTO GENERATED CODE -------------- +*/


  /**
   * This constructor is called by the processReceive method of
   * BAdMonitorDriverReadRequest.
   *
   * @param a reference to the data frame that matches up with
   * the request that was recently transmitted. The byte array
   * in this frame could be a direct reference to the dev
   * communicator's receiver's internal byte array so this
   * constructor copies the bytes into a safe instance array.
   * 
   * @param a reference to the read parameters structure that the
   * read request used to construct its own byte array.
   * 
   * @throws DdfResponseException if the given frame contains
   * information from the field-device that indicates that the
   * read request was unsuccessful or otherwise denied.
   */
  public BAdMonitorDriverReadResponse(IDdfDataFrame receiveFrame, BAdMonitorDriverReadParams readParams)
    throws DdfResponseException
  {
           
    receiveBytes = new byte [receiveFrame.getFrameSize()];
    
    //copy the receive frame as recommended 
    System.arraycopy(receiveFrame.getFrameBytes(), 0, receiveBytes, 0, receiveFrame.getFrameSize());
    
  }

  /**
   * This empty constructor allows Niagara AX to instantiate
   * a client-side proxy of this request. It is not presently
   * used but could be required in future versions of the
   * developer driver framework.
   */
  public BAdMonitorDriverReadResponse()
  {

  }

  /**
   * Implementing this method is fundamental to the ddf's
   * auto-poll feature for driver points. When one or more driver points
   * under a device need polled that share the equivalent read parameters,
   * the ddf will instantiate the read request type that
   * is identified by the read parameters, assign the read parameters to the
   * read request, assign all points that share the read parameters to the
   * request, and transmit the read request. Upon receiving a successful
   * read response (an instance that implements this interface), the ddf
   * driver framework will loop through all of the points under the device
   * that shared the same read parameters, cast each point to IDdfReadable,
   * and pass each point successively to this method. The developer driver
   * framework will take the return value from this method and pass it
   * to the readOk method on the point, thereby updating its value in Niagara.
   *
   * When implementing this interface, driver developers must implement this
   * method and parse a BStatusValue from the response data for the given
   * readableSource. If necessary, we suggest that the driver developer can
   * check if the readableSource object is an instance of their driver's proxy
   * extension class. If so, the driver developer can cast the readableSource
   * object to their driver's proxy extension class and then access the point's
   * pointId property. The driver developer should design the pointId property
   * in such a way that it provides the information necessary to parse the
   * particular point's value from the read response.
   *
   *
   * @param readableSource
   * @return a BStatusValue to pass to the readOk method of the readableSource
   */
  private String [] rawValues = null;
  public BStatusValue parseReadValue(IDdfReadable readableSource)
  { 
    // Verify that the given readableSource is an instance of AdMonitorDriver
    // driver's proxy extension
    
    
    if (readableSource instanceof BAdMonitorDriverProxyExt)
    { 
      // Casts the given readableSource into a BAdMonitorDriverProxyExt
      BAdMonitorDriverProxyExt proxy
        = (BAdMonitorDriverProxyExt)readableSource;
      
      // Gets the point id of the given proxy
      BAdMonitorDriverPointId pointId
        = (BAdMonitorDriverPointId)proxy.getPointId();
      
      
      if (rawValues==null)
        parseRawValues();
      
      return getReadValue(proxy,pointId);
    }
    else
    {
      return null;
    }
  }
  
  private void parseRawValues()
  {
    // TODO Auto-generated method stub
    
  }

  /**
   * This private method is called from the parseReadValue method.
   * It is not required by the developer driver framework, instead, 
   * this method exists in order to shorten the parseReadValue method.
   *
   * @param a reference to the BAdMonitorDriverProxyExt to parse the read
   * value for.
   *
   * @param a reference to the BAdMonitorDriverPointId to use to tell us
   * how to parse the read value from the response bytes.
   *
   * @return a BStatusNumeric, BStatusBoolean, BStatusEnum, or
   * BStatusString that appropriately matches the proxy's control
   * point type. If the proxy's control point is a BNumericWritable
   * or BNumericPoint then this will return a BStatusNumeric that
   * represents the present value of the point. If the proxy's
   * control point is a BBooleanWritable or a BBooleanPoint then
   * this returns a BStatusBoolean that represents the current value
   * of the point. If the proxy's control point is a BEnumWritable
   * or BEnumPoint then this returns BStatusEnum that represents the
   * current value of the point. If this proxy's control point is a
   * BStringPoint or BStringWritable then this returns a BStatusString
   * that represents the current value of the point.
   */
  private BStatusValue getReadValue(BAdMonitorDriverProxyExt proxy,
      BAdMonitorDriverPointId pointId)
  {
    
  /*
   * String controllerId = pointId.getName(); //getID eprepe na paro. Den yparxei omos kai prepei na to ftiakso.
   *
    String content = new String (receiveBytes).trim();
    
    content = content.substring("get ".length());
    StringTokenizer tokenizer = new StringTokenizer(content, "\t");
    
    while (tokenizer.hasMoreTokens())
    {
      String token = tokenizer.nextToken();
      if (controllerId.equalsIgnoreCase(token.substring(0, token.indexOf(" "))));
         // blah blah blah
     
    
     
    else throw new IllegalStateException();
      
    }
    
    *
    *
    */
    
    
    
    
    // HINT: This code is merely a suggestion. Please use it as a starting
    // point.
    
    // Gets the raw value for the given proxy
    String sRawValue = "";// TODO: Parse the bytes that are cached in the constructor
      
    // Normalizes the string raw value into an int
    int iRawValue = 0;
    if (sRawValue.equalsIgnoreCase("on"))
      iRawValue = 1;
    else if (sRawValue.equalsIgnoreCase("off"))
      iRawValue = 0;
    else
      iRawValue = Integer.parseInt(sRawValue);

    // Wraps iRawValue into a BStatusValue of the appropriate type
    // For the control point
    BControlPoint controlPoint = proxy.getParentPoint();
    
    // Checks if the control point is a Numeric Writable or Numeric
    // Point component.
    if (controlPoint instanceof BINumeric)
    { 
      // This is all we need to do! Any scales, offsets, or other
      // Conversions are handled by other parts of Niagara. We just
      // Need to return a raw representation here.
      return new BStatusNumeric(iRawValue);
    }
    // Checks if the control point is a Boolean Writable or a Boolean
    // Point component. NOTE: We must check boolean before enum because
    // a BIBoolean is also a BIEnum!
    else if (controlPoint instanceof BIBoolean)
    { 
      // This is all we need to do. Any polarity conversion, etc. is
      // specified and handled elsewhere in Niagara AX
      return new BStatusBoolean(iRawValue>0);
    }
    // Checks if the control point is an Enum Writable or an Enum Point
    // component.
    else if (controlPoint instanceof BIEnum)
    { 
      // A slight extra step is imperative for Enum Points to preserve
      // any dynamic enum range (text to ordinal mapping).
      BStatusEnum e = ((BEnumPoint)controlPoint).getOut();
      return new BStatusEnum( BDynamicEnum.make(
        (int)Math.round(iRawValue),
        e.getValue().getRange()));
    }
    else if (controlPoint instanceof BStringPoint)
    {
      return new BStatusString(sRawValue);
    }
    else
    {
      throw new IllegalStateException(
        "Unsupported control point type: "+
        controlPoint.getType()+"! Please have my program fixed.");
    }

  }
  
  
  
  
  
    
////////////////////////////////////////////////////////////////
// Attributes
////////////////////////////////////////////////////////////////
  
  private byte[] receiveBytes; // This is populated by the constructor
  private BAdMonitorDriverReadParams readParams; // This is populated by the constructor

}