/*
 * Copyright 2014 Lerch Bates Ltd, All Rights Reserved.
 */
package com.lerchBatesLtd.adMonitorDriver.comm;

import javax.baja.sys.*;

import com.tridium.ddf.comm.*;
import com.tridium.ddf.comm.defaultComm.*;
import com.tridium.ddfSerial.comm.*;

public class BAdMonitorDriverReceiver
  extends BDdfSerialReceiver{

  /*-
  class BAdMonitorDriverReceiver
  {
  }
  -*/
/*+ ------------ BEGIN BAJA AUTO GENERATED CODE ------------ +*/
/*@ $com.lerchBatesLtd.adMonitorDriver.comm.BAdMonitorDriverReceiver(3454642592)1.0$ @*/
/* Generated Tue Jan 21 13:08:13 GMT 2014 by Slot-o-Matic 2000 (c) Tridium, Inc. 2000 */

////////////////////////////////////////////////////////////////
// Type
////////////////////////////////////////////////////////////////
  
  public Type getType() { return TYPE; }
  public static final Type TYPE = Sys.loadType(BAdMonitorDriverReceiver.class);

/*+ ------------ END BAJA AUTO GENERATED CODE -------------- +*/


////////////////////////////////////////////////////////////////
// Methods
////////////////////////////////////////////////////////////////

  /**
   * This method is called by the default implementation of
   * BDdfReceiver. Its purpose is to recognize the starting
   * sequence of a data packet according to the driver's
   * protocol.
   * 
   * Descendants should override this method, and check the data
   * received so far. If the data received so far indicates that
   * this is the beginning of a data frame then the subclass
   * should return BDdfReceiver.YES. If the data cannot possibly
   * be the beginning of the frame for the driver then the
   * subclass should return BDdfReceiver.NO. If the descendant
   * needs more data before making a decision then it should
   * return BDdfReciver.MAYBE.
   * 
   * @param frameSoFar is the IDdfDataFrame that the reciever
   * is building up.
   * 
   * @return BDdfReciver.YES if the bytes that are in the given
   * frameSoFar are the start of a frame. Or BDdfReceiver.NO if
   * the bytes in the given frameSoFar cannot possibly be the
   * start of a frame. Or BDdfReciver.MAYBE if the bytes in the
   * given frameSoFar could be the start of a frame but more
   * bytes would be required to make a definite determination.
   */
  public int isStartOfFrame(IDdfDataFrame frameSoFar)
  {
    // TODO: Examine the bytes in the given data frame. The goal
    // is to detect whether or not the bytes that are in this
    // data frame so far would indicate the beginning of a packet
    // according to the driver's particular protocol. This could
    // be as simple as checking whether the first character is
    // a hex 0x01 or it could be quite complicated.
    
    // TODO: Return BDdfReceiver.NO if the bytes that are in the
    // data frame so far do not represent the start of a packet
    // per your driver's protocol.
    
    // TODO: Return BDdfReceiver.YES if the bytes that are in the
    // data fame so far do in fact match the starting sequence
    // that your driver's protocol requires.
    
    // TODO: Return BDdfReceiver.MAYBE if you would need more bytes
    // in order to make a decision as to whether or not the bytes
    // that are in the data frame so far do in fact match the
    // starting sequence that your driver's protocol requires.
    // data frame so far are not
    
    // NOTE: If you return BDdfReceiver.NO then the data frame will
    // be reset. The next time that a byte is received, it will be
    // the first byte in the given data frame.
    
    // NOTE: If you return BDdfReceiver.YES then the default
    // implementation of BDdfReceiver will change its internal
    // state so that subsequently received bytes will continue to
    // be placed in the data frame. However, the data frame will
    // be passed to the 'isCompleteFrame' method on this class
    // instead.
    byte [] msg = frameSoFar.getFrameBytes();
    if (msg[0] == 0x01)
      return BDdfReceiver.YES;
    else
    return BDdfReceiver.NO; // To CHANGE
  }

  /**
   * This method is called by the default implementation of
   * BDdfReceiver. Its purpose is to recognize the ending
   * sequence of a data packet according to the driver's
   * protocol.
   * 
   * Descendants should override this method, and check
   * the data received so far. If the data received so
   * far indicates that this is a completed frame then
   * the subclass should return true. Otherwise, this
   * method should return false so that the receiver
   * can keep receiving and buffering data.
   * 
   * Descendants should override this method, and check
   * the data received so far. If the data received so
   * far indicates that this is a completed frame then
   * the subclass should return true. Otherwise, this
   * method should return false so that the receiver
   * can keep receiving and buffering data.
   */
  public boolean isCompleteFrame(IDdfDataFrame frameSoFar)
  {
    // TODO: Review the bytes that are in the given frameSoFar
    // and determine whether or not the frame would constitute
    // a complete packet of data per your driver's protocol.
    // This could be as simple as checking if the last byte
    // is, for example, hex 0x04. Alternatively, some
    // protocols might declare that a particular offset into
    // the byte array indicates the 'length' of the packet.
    // In this case, this method would get the 'length' byte
    // and count the number of bytes in the given frameSoFar.
    // Good luck!
    
    // NOTE: If this method returns 'true' then the default
    // implementation of BDdfReceiver will immediately call the
    // 'checkFrame' method on this class where you may
    // perform integrity checks (data validation).
    
    // NOTE: If this method returns 'false' then the default
    // implementation of BDdfReceiver will continue calling
    // this method and passing in the working frameSoFar with
    // subsequently received bytes until this method returns
    // true.
    
    // NOTE: If you detect a problem with the dataframe, return
    // true... The default implementation of devDriver will
    // immediately call 'checkFrame' where you should then
    // return 'false' to indicate that the frame was invalid.
    byte[] msg = frameSoFar.getFrameBytes();
    return  msg[frameSoFar.getFrameSize() - 1] == 23; // toChange. OTIS protocol says EOT=256?   
    //((frameSoFar.getFrameBytes() > 2 ) should I define this?
  }

  /**
   * This method is called by the receiver after the isCompleteFrame message
   * returns true.
   * @param completeFrame the complete IDdfDataFrame received
   * @return true if the completeFrame passes check sum tests, or if no check sum
   * test is necessary (data in TCP/IP messages, for example, do not alwasy require
   * a separate check sum test since checking is built into TCP/IP). False if a check
   * is necessary and the check fails.
   */
  public boolean checkFrame(IDdfDataFrame frameSoFar)
  {
    // TODO: The given 'frameSoFar' is the same 'frameSoFar' that
    // your 'isCompleteFrame' method would have just returned 'true'
    // for. Please review this frame further and verify that it
    // passes any data integrity checks such as a check sum, CRC,
    // or LRC that your driver's protocol may require.
    
    //return (frameSoFar.getFrameBytes()[0] == 0x01) && (frameSoFar.getFrameBytes()[frameSoFar.getFrameSize()-1 == 0x17]);
    
    return true; 
  }
}