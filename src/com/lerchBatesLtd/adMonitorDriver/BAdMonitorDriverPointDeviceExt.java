/*
 * Copyright 2014 Lerch Bates Ltd, All Rights Reserved.
 */
package com.lerchBatesLtd.adMonitorDriver;

import javax.baja.sys.*;
import javax.baja.util.*;

import com.tridium.ddf.*;
import com.tridium.ddf.discover.*;

import com.lerchBatesLtd.adMonitorDriver.point.*;
import com.lerchBatesLtd.adMonitorDriver.discover.*;

public class BAdMonitorDriverPointDeviceExt
  extends BDdfPointDeviceExt
{
  /*-
  class BAdMonitorDriverPointDeviceExt
  {
    properties
    {
      discoveryPreferences : BDdfDiscoveryPreferences
        -- This saves the last set of discovery parameters that the user provided.
        -- It also allows the easy driver framework to automatically learn points
       flags{hidden}
       default{[ new BAdMonitorDriverPointDiscoveryPreferences()]}
    }
  }
  -*/
/*+ ------------ BEGIN BAJA AUTO GENERATED CODE ------------ +*/
/*@ $com.lerchBatesLtd.adMonitorDriver.BAdMonitorDriverPointDeviceExt(926329734)1.0$ @*/
/* Generated Wed Jan 22 18:00:47 GMT 2014 by Slot-o-Matic 2000 (c) Tridium, Inc. 2000 */

////////////////////////////////////////////////////////////////
// Property "discoveryPreferences"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the <code>discoveryPreferences</code> property.
   * This saves the last set of discovery parameters that
   * the user provided. It also allows the easy driver framework to automatically learn points
   * @see com.lerchBatesLtd.adMonitorDriver.BAdMonitorDriverPointDeviceExt#getDiscoveryPreferences
   * @see com.lerchBatesLtd.adMonitorDriver.BAdMonitorDriverPointDeviceExt#setDiscoveryPreferences
   */
  public static final Property discoveryPreferences = newProperty(Flags.HIDDEN, new BAdMonitorDriverPointDiscoveryPreferences(),null);
  
  /**
   * Get the <code>discoveryPreferences</code> property.
   * This saves the last set of discovery parameters that
   * the user provided. It also allows the easy driver framework to automatically learn points
   * @see com.lerchBatesLtd.adMonitorDriver.BAdMonitorDriverPointDeviceExt#discoveryPreferences
   */
  public BDdfDiscoveryPreferences getDiscoveryPreferences() { return (BDdfDiscoveryPreferences)get(discoveryPreferences); }
  
  /**
   * Set the <code>discoveryPreferences</code> property.
   * This saves the last set of discovery parameters that
   * the user provided. It also allows the easy driver framework to automatically learn points
   * @see com.lerchBatesLtd.adMonitorDriver.BAdMonitorDriverPointDeviceExt#discoveryPreferences
   */
  public void setDiscoveryPreferences(BDdfDiscoveryPreferences v) { set(discoveryPreferences,v,null); }

////////////////////////////////////////////////////////////////
// Type
////////////////////////////////////////////////////////////////
  
  public Type getType() { return TYPE; }
  public static final Type TYPE = Sys.loadType(BAdMonitorDriverPointDeviceExt.class);

/*+ ------------ END BAJA AUTO GENERATED CODE -------------- +*/

  /**
   * Associates BAdMonitorDriverPointDeviceExt to BAdMonitorDriverDevice.
   */
  public Type getDeviceType()
  {
    return BAdMonitorDriverDevice.TYPE;
  }
  
  /**
   * Associates BAdMonitorDriverPointDeviceExt to BAdMonitorDriverPointFolder.
   */
  public Type getPointFolderType()
  {
    return BAdMonitorDriverPointFolder.TYPE;
  }
  
  /**
   * Associates BAdMonitorDriverPointDeviceExt to BAdMonitorDriverProxyExt
   */
  public Type getProxyExtType()
  {
    return BAdMonitorDriverProxyExt.TYPE;
  }
  
  /**
   * This can be left null, depending on how you decide to define
   * the discovery behavior in your driver. We will visit the
   * discovery process in further detail during another day's
   * lesson.
   */
  public BFolder getDiscoveryFolder()
  {
    return null;
  }
  
}