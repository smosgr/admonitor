/*
 * Copyright 2014 Lerch Bates Ltd, All Rights Reserved.
 */

package com.lerchBatesLtd.adMonitorDriver;

import javax.baja.sys.*;

import com.tridium.ddf.identify.BDdfIdParams;

import com.tridium.ddfSerial.*;

import com.lerchBatesLtd.adMonitorDriver.comm.*;
import com.lerchBatesLtd.adMonitorDriver.identify.BAdMonitorDriverDeviceId;


public class BAdMonitorDriverDevice
  extends BDdfSerialDevice
{
  /*-
  class BAdMonitorDriverDevice
  {
    properties
    {
                
                
      deviceId : BDdfIdParams
        -- This plugs in an instance of AdMonitorDriver's
        -- device id as this device's deviceId
        default {[new BAdMonitorDriverDeviceId()]}
        slotfacets{[MGR_INCLUDE]}
      points : BAdMonitorDriverPointDeviceExt
        -- Adds the special point device extension
        -- component to the property sheet and the
        -- Niagara AX navigation tree. This special
        -- component will contain and process all
        -- control points for YourDriver
        default{[new BAdMonitorDriverPointDeviceExt()]}
    }
  }
  -*/
/*+ ------------ BEGIN BAJA AUTO GENERATED CODE ------------ +*/
/*@ $com.lerchBatesLtd.adMonitorDriver.BAdMonitorDriverDevice(2819682154)1.0$ @*/
/* Generated Tue Jan 21 13:08:13 GMT 2014 by Slot-o-Matic 2000 (c) Tridium, Inc. 2000 */

////////////////////////////////////////////////////////////////
// Property "deviceId"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the <code>deviceId</code> property.
   * This plugs in an instance of AdMonitorDriver's device
   * id as this device's deviceId
   * @see com.lerchBatesLtd.adMonitorDriver.BAdMonitorDriverDevice#getDeviceId
   * @see com.lerchBatesLtd.adMonitorDriver.BAdMonitorDriverDevice#setDeviceId
   */
  public static final Property deviceId = newProperty(0, new BAdMonitorDriverDeviceId(),MGR_INCLUDE);
  
  /**
   * Get the <code>deviceId</code> property.
   * This plugs in an instance of AdMonitorDriver's device
   * id as this device's deviceId
   * @see com.lerchBatesLtd.adMonitorDriver.BAdMonitorDriverDevice#deviceId
   */
  public BDdfIdParams getDeviceId() { return (BDdfIdParams)get(deviceId); }
  
  /**
   * Set the <code>deviceId</code> property.
   * This plugs in an instance of AdMonitorDriver's device
   * id as this device's deviceId
   * @see com.lerchBatesLtd.adMonitorDriver.BAdMonitorDriverDevice#deviceId
   */
  public void setDeviceId(BDdfIdParams v) { set(deviceId,v,null); }

////////////////////////////////////////////////////////////////
// Property "points"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the <code>points</code> property.
   * Adds the special point device extension component to the property sheet and the Niagara AX navigation tree. This special component will contain and process all control points for YourDriver
   * @see com.lerchBatesLtd.adMonitorDriver.BAdMonitorDriverDevice#getPoints
   * @see com.lerchBatesLtd.adMonitorDriver.BAdMonitorDriverDevice#setPoints
   */
  public static final Property points = newProperty(0, new BAdMonitorDriverPointDeviceExt(),null);
  
  /**
   * Get the <code>points</code> property.
   * Adds the special point device extension component to the property sheet and the Niagara AX navigation tree. This special component will contain and process all control points for YourDriver
   * @see com.lerchBatesLtd.adMonitorDriver.BAdMonitorDriverDevice#points
   */
  public BAdMonitorDriverPointDeviceExt getPoints() { return (BAdMonitorDriverPointDeviceExt)get(points); }
  
  /**
   * Set the <code>points</code> property.
   * Adds the special point device extension component to the property sheet and the Niagara AX navigation tree. This special component will contain and process all control points for YourDriver
   * @see com.lerchBatesLtd.adMonitorDriver.BAdMonitorDriverDevice#points
   */
  public void setPoints(BAdMonitorDriverPointDeviceExt v) { set(points,v,null); }

////////////////////////////////////////////////////////////////
// Type
////////////////////////////////////////////////////////////////
  
  public Type getType() { return TYPE; }
  public static final Type TYPE = Sys.loadType(BAdMonitorDriverDevice.class);

/*+ ------------ END BAJA AUTO GENERATED CODE -------------- +*/
   
  public Type getNetworkType()
  {
    return BAdMonitorDriverNetwork.TYPE;
  }
}